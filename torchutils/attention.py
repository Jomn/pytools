import torch
from torch import nn
from torch.nn import functional as F
from pytools.torchutils import utils


class SelfAttention(nn.Module):
    def __init__(self, hidden_size, cuda=False):
        super(SelfAttention, self).__init__()
        self.use_cuda = cuda
        self.hidden_size = hidden_size

        self.linear = nn.Linear(self.hidden_size, self.hidden_size)
        self.context_weights = nn.Parameter(torch.Tensor(self.hidden_size, 1))

        self._init_weights()

    def _init_weights(self):
        initrange = 0.1
        self.linear.bias.data.fill_(0)
        self.linear.weight.data.uniform_(-initrange, initrange)
        self.context_weights.data.uniform_(-initrange, initrange)

    def forward(self, annotations, lengths=None):
        # annotations: batch_size x seq_len x hidden_size
        # context_weights: hidden_size x 1

        score = torch.tanh(self.linear(annotations))
        # score: batch_size x seq_len x hidden_size
        score = torch.matmul(score, self.context_weights).squeeze(2)

        if lengths is not None:
            max_len = annotations.size(1)
            idxes = utils.gpu(torch.arange(0, max_len, out=torch.LongTensor(max_len)).unsqueeze(0), self.use_cuda)
            mask = idxes.ge(lengths.unsqueeze(1))
            score.data.masked_fill_(mask, -float('inf'))

        # score: batch_size x seq_len
        attn_weights = F.softmax(score, dim=-1)

        # if lengths is not None:
        #     # mask = utils.gpu(Variable(torch.ones(attn_weights.size())), self.use_cuda)
        #     max_len = attn_weights.size(1)
        #     idxes = utils.gpu(torch.arange(0, max_len, out=torch.LongTensor(max_len)).unsqueeze(0), self.use_cuda)
        #     mask = Variable((idxes.lt(lengths.unsqueeze(1))).float(), requires_grad=False)
        #     masked = attn_weights * mask
        #     _sums = masked.sum(-1).unsqueeze(1).expand(masked.size())
        #     attn_weights = masked.div(_sums)

        # attn_weights: batch_size x seq_len
        context = attn_weights.unsqueeze(1).bmm(annotations).squeeze(1)

        # context: batch_size x hidden_size
        return context, attn_weights


class MultiHeadSelfAttention(nn.Module):
    def __init__(self, hidden_size, n_heads=1, cuda=False):
        super(MultiHeadSelfAttention, self).__init__()
        self.use_cuda = cuda
        self.hidden_size = hidden_size

        self.linear = nn.Linear(self.hidden_size, self.hidden_size)
        self.context_weights = nn.Parameter(torch.Tensor(n_heads, self.hidden_size))

        self._init_weights()

    def _init_weights(self):
        initrange = 0.1
        self.linear.bias.data.fill_(0)
        self.linear.weight.data.uniform_(-initrange, initrange)
        self.context_weights.data.uniform_(-initrange, initrange)

    def forward(self, annotations, lengths=None):
        # annotations: batch_size x seq_len x hidden_size
        # context_weights: n_heads x hidden_size

        score = torch.tanh(self.linear(annotations))
        # score: batch_size x seq_len x hidden_size
        score = torch.matmul(self.context_weights, score.transpose(2, 1))

        if lengths is not None:
            max_len = annotations.size(1)
            idxes = utils.gpu(torch.arange(0, max_len, out=torch.LongTensor(max_len)).unsqueeze(0), self.use_cuda)
            mask = idxes.ge(lengths.unsqueeze(1))
            score.data.masked_fill_(mask, -float('inf'))

        # score: batch_size x n_heads x seq_len
        attn_weights = F.softmax(score, dim=-1)

        # attn_weights: batch_size x n_heads x seq_len
        context = attn_weights.bmm(annotations)

        # context: batch_size x n_heads x hidden_size
        return context, attn_weights


class Attention(nn.Module):
    def __init__(self, hidden_size, cuda=False):
        super(Attention, self).__init__()

        self.hidden_size = hidden_size
        self.use_cuda = cuda

    def forward(self, target_state, annotations, lengths=None, mask=None):
        """
        target_state: batch_size x hidden_dim
        annotations: batch_size x seq_len x hidden_dim
        """

        scores = self.score(target_state, annotations)
        # scores: batch_size x seq_len

        # Mask unwanted data
        if lengths is not None:
            max_len = annotations.size(1)
            idxes = utils.gpu(torch.arange(0, max_len, out=torch.LongTensor(max_len)).unsqueeze(0), self.use_cuda)
            mask = idxes.ge(lengths.unsqueeze(1))

        if mask is not None:
            scores.data.masked_fill_(mask, -float('inf'))

        # Normalize energies to weights in range 0 to 1, resize to batch x 1 x seq_len
        attn_weights = F.softmax(scores, dim=-1)
        context = attn_weights.unsqueeze(1).bmm(annotations).squeeze(1)

        return context, attn_weights

    def score(self, target_state, annotations):
        raise NotImplementedError("Attention is an abstract class!")


class MulAttention(Attention):
    def __init__(self, hidden_size, cuda=False):
        super(MulAttention, self).__init__(hidden_size, cuda)
        self.linear = nn.Linear(self.hidden_size, hidden_size, bias=False)
        self._init_weights()

    def _init_weights(self):
        initrange = 0.1
        # self.linear.bias.data.fill_(0)
        self.linear.weight.data.uniform_(-initrange, initrange)

    def score(self, target_state, annotations):
        # target_state: batch_size x hidden_dim
        # annotations: batch_size x seq_len x hidden_dim

        energy = self.linear(annotations)
        energy = torch.bmm(energy, target_state.unsqueeze(2))
        # energy : batch_size x seq_len x 1
        return energy.squeeze(2)


class AddAttention(Attention):
    def __init__(self, hidden_size, cuda=False):
        super(AddAttention, self).__init__(hidden_size, cuda)
        self.linear1 = nn.Linear(self.hidden_size, self.hidden_size, bias=False)
        self.linear2 = nn.Linear(self.hidden_size, self.hidden_size, bias=False)
        self.context_weights = nn.Parameter(torch.Tensor(self.hidden_size, 1))
        self._init_weights()

    def _init_weights(self):
        initrange = 0.1
        # self.linear1.bias.data.fill_(0)
        self.linear1.weight.data.uniform_(-initrange, initrange)
        # self.linear2.bias.data.fill_(0)
        self.linear2.weight.data.uniform_(-initrange, initrange)
        self.context_weights.data.uniform_(-initrange, initrange)

    def score(self, target_state, annotations):
        # target_state: batch_size x hidden_dim
        # annotations: batch_size x seq_len x hidden_dim

        score = torch.tanh(self.linear1(annotations) + self.linear2(target_state.unsqueeze(1)))
        # score: batch_size x seq_len x hidden_size
        score = torch.matmul(score, self.context_weights).squeeze(2)
        # score: batch_size x seq_len
        return score


class MixedAttention(nn.Module):
    def __init__(self, hidden_size, cuda=False):
        super(MixedAttention, self).__init__()

        self.hidden_size = hidden_size
        self.use_cuda = cuda

    def forward(self, target_annotation, annotations, states, lengths=None, mask=None):
        """
        target_annotation: batch_size x hidden_dim
        annotations: batch_size x seq_len x hidden_dim
        states: batch_size x seq_len x state_dim
        """

        scores = self.score(target_annotation, annotations)
        # scores: batch_size x seq_len

        # Mask unwanted data
        if lengths is not None:
            max_len = states.size(1)
            idxes = utils.gpu(torch.arange(0, max_len, out=torch.LongTensor(max_len)).unsqueeze(0), self.use_cuda)
            mask = idxes.ge(lengths.unsqueeze(1))

        if mask is not None:
            scores.data.masked_fill_(mask, -float('inf'))

        # Normalize energies to weights in range 0 to 1, resize to batch x 1 x seq_len
        attn_weights = F.softmax(scores, dim=-1)
        # attn_weights: batch_size x seq_len
        context = attn_weights.unsqueeze(1).bmm(states).squeeze(1)
        # context: batch_size x state_dim

        return context, attn_weights

    def score(self, target_annotation, annotations):
        raise NotImplementedError("Attention is an abstract class!")


class MulMixedAttention(MixedAttention):
    def __init__(self, hidden_size, cuda=False):
        super(MulMixedAttention, self).__init__(hidden_size, cuda)
        self.linear = nn.Linear(hidden_size, hidden_size, bias=False)
        self._init_weights()

    def _init_weights(self):
        initrange = 0.1
        # self.linear.bias.data.fill_(0)
        self.linear.weight.data.uniform_(-initrange, initrange)

    def score(self, target_annotation, annotations):
        # target_annotation: batch_size x hidden_dim
        # annotations: batch_size x seq_len x hidden_dim

        energy = self.linear(annotations)
        energy = torch.bmm(energy, target_annotation.unsqueeze(2))
        # energy : batch_size x seq_len x 1
        return energy.squeeze(2)


class AddMixedAttention(MixedAttention):
    def __init__(self, hidden_size, cuda=False):
        super(AddMixedAttention, self).__init__(hidden_size, cuda)
        self.linear1 = nn.Linear(self.hidden_size, self.hidden_size, bias=False)
        self.linear2 = nn.Linear(self.hidden_size, self.hidden_size, bias=False)
        self.context_weights = nn.Parameter(torch.Tensor(self.hidden_size, 1))
        self._init_weights()

    def _init_weights(self):
        initrange = 0.1
        # self.linear1.bias.data.fill_(0)
        self.linear1.weight.data.uniform_(-initrange, initrange)
        # self.linear2.bias.data.fill_(0)
        self.linear2.weight.data.uniform_(-initrange, initrange)
        self.context_weights.data.uniform_(-initrange, initrange)

    def score(self, target_annotation, annotations):
        # target_annotation: batch_size x hidden_dim
        # annotations: batch_size x seq_len x hidden_dim

        score = torch.tanh(self.linear1(annotations) + self.linear2(target_annotation.unsqueeze(1)))
        # score: batch_size x seq_len x hidden_size
        score = torch.matmul(score, self.context_weights).squeeze(2)
        # score: batch_size x seq_len
        return score
