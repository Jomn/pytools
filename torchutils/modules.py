import torch
from torch import nn


class CudaModule(nn.Module):
    def __init__(self, use_cuda: bool):
        super().__init__()
        self.use_cuda = use_cuda

    def set_cuda(self, use_cuda: bool):
        self.use_cuda = use_cuda
        for child in self.children():
            try:
                child.set_cuda(use_cuda)
            except AttributeError:
                pass

    def on_cuda(self):
        self.set_cuda(True)

    def on_cpu(self):
        self.set_cuda(False)
