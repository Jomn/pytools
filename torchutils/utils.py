import collections
import time
import math

import numpy as np
import torch
from torch import nn, optim
from torch.autograd import Variable
from tqdm import tqdm

from pytools.h5tools import h5tools
from pytools.helpers import nptools


def gpu(tensor, use_cuda):
    return tensor.cuda() if use_cuda else tensor


def ungpu(tensor, use_cuda):
    return tensor.cpu() if use_cuda else tensor


def get_device(use_cuda):
    return torch.device("cuda") if use_cuda else torch.device("cpu")


def numpy_to_tensors(np_array, cuda=False, variable=False):
    """Utility function to convert numpy arrays to torch tensors.

    :param np_array: Numpy array to convert to a torch tensor.
    :param cuda: Flag to indicate if the tensor should use a cuda device.
    :param variable: Deprecated. Useless in PyTorch 0.4.1.
    :return: The torch tensor.
    """
    return gpu(torch.from_numpy(np_array), cuda)


def npz_to_tensors(npz, datanames, raise_nonexistent=True):
    """ Loads numpy arrays from the NPZ and transforms numpy arrays into torch tensors.
    """
    tensors = []
    for input_name in datanames:
        if input_name not in npz:
            if raise_nonexistent:
                raise RuntimeError(f"'{input_name}' doesn't exist in NPZ file")
            else:
                continue
        tensors.append(torch.from_numpy(npz[input_name]))

    return tuple(tensors)


def numpy_to_variable(datalist, cuda=False):
    """Convert numpy arrays to torch Variable tensors

    .. note:: This function is deprecated. Variables have no use in PyTorch 0.4.1.
    """
    return [gpu(Variable(torch.from_numpy(data)), cuda) for data in datalist]


def npz_filepath_to_tensors(npz_filepath, input_names, output_names, raise_nonexistent=True):
    """ Loads a npz file and transforms numpy arrays into torch tensors.
    """
    npz = np.load(npz_filepath)
    inputs = []
    for input_name in input_names:
        if input_name not in npz:
            if raise_nonexistent:
                raise RuntimeError("'{}' doesn't exist in NPZ file '{}'".format(input_name, npz_filepath))
            else:
                continue
        inputs.append(torch.from_numpy(npz[input_name]))

    outputs = []
    for output_name in output_names:
        if output_name not in npz:
            if raise_nonexistent:
                raise RuntimeError("'{}' doesn't exist in NPZ file '{}'".format(output_name, npz_filepath))
            else:
                continue
        outputs.append(torch.from_numpy(npz[output_name]))

    return (tuple(inputs), tuple(outputs))


def standardize_features(tensor, mean=None, std=None, return_meanstd=False):
    if mean is None:
        mean = torch.mean(tensor, dim=0)
    if std is None:
        std = torch.std(tensor, dim=0)
    if return_meanstd:
        return (tensor - mean) / std, mean, std
    return (tensor - mean) / std


def h5_to_tensors(h5, datanames, raise_nonexistent=True):
    """ Loads a hdf5 file and transforms it into torch tensors.
    """
    tensors = []
    for input_name in datanames:
        if input_name not in h5:
            if raise_nonexistent:
                raise RuntimeError(f"'{input_name}' doesn't exist in H5 file")
            else:
                continue
        tensors.append(torch.from_numpy(h5[input_name][()]))

    return tuple(tensors)


# def indices_by_group(groups, group_indices, batch_size):
#     indices = []
#     group_batches_size = []
#     group_progress = collections.defaultdict(int)
#     for group_index in group_indices:
#         progress = group_progress[group_index]
#         start = progress * batch_size
#         end = progress * batch_size + batch_size
#         indices.extend(groups[group_index][start:end])
#         if end > len(groups[group_index]):
#             group_batches_size.append(len(groups[group_index]) - start)
#         else:
#             group_batches_size.append(batch_size)
#         group_progress[group_index] += 1
#     indices = torch.tensor(indices)
#     return indices, group_batches_size


def make_batch(datalist, batch_size, batch, indices=None, cuda=False):
    """ Creates batches of the given data.
    The final batch may be smaller than the batch size.
    Args:
      batch_size (int): Wanted size for the batches
      datalist: List of Numpy arrays containing the data to batch
      batch (int): Index of the wanted batch
      indices (tensor): Shuffled indices to use to create the batch
      cuda (bool): Are we using a GPU ?
    Returns a list of batches
    """
    batch_list = []
    for data in datalist:
        if indices is None:
            batch_list.append(gpu(Variable(data[batch:batch+batch_size]), cuda))
        else:
            batch_list.append(gpu(Variable(data[indices[batch:batch+batch_size]]), cuda))

    return batch_list


# def make_batch_by_group(datalist, start, indices, batch_idx, group_batches_size, cuda=False):
#     batch_list = []
#     for data in datalist:
#         batch_list.append(gpu(Variable(data[indices[start:start+group_batches_size[batch_idx]]]), cuda))
#     return batch_list


def make_batch_dict(datadict, batch_size, batch, indices=None, cuda=False):
    """ Creates batches of the given data.
    The final batch may be smaller than the batch size.
    Args:
      batch_size (int): Wanted size for the batches
      datadict: Dict of Numpy arrays containing the data to batch
      batch (int): Index of the wanted batch
      indices (tensor): Shuffled indices to use to create the batch
      cuda (bool): Are we using a GPU ?
    Returns a dict of batches
    """
    batch_dict = {}
    for data_name, data in datadict.items():
        if indices is None:
            batch_dict[data_name] = gpu(Variable(data[batch:batch + batch_size]), cuda)
        else:
            batch_dict[data_name] = gpu(Variable(data[indices[batch:batch + batch_size]]), cuda)

    return batch_dict


def make_lengths_batch(lengthslist, batch_size, batch, indices=None, cuda=False):
    batch_list = []
    for lengths in lengthslist:
        if indices is None:
            batch_list.append(gpu(lengths[batch:batch + batch_size], cuda))
        else:
            batch_list.append(gpu(lengths[indices[batch:batch + batch_size]], cuda))

    return batch_list


# def make_lengths_batch_by_group(lengthslist, start, indices, batch_idx, group_batches_size, cuda=False):
#     batch_list = []
#     for data in lengthslist:
#         batch_list.append(gpu(data[indices[start:start+group_batches_size[batch_idx]]], cuda))
#     return batch_list


def shuffle_indices(data_length):
    return torch.randperm(data_length)


def select_optimizer(mdl, optimizer_name, learning_rate):
    if(optimizer_name == 'Adam'):
        optimizer = optim.Adam(mdl.parameters(), lr=learning_rate)
    elif(optimizer_name == 'RMS'):
        optimizer = optim.RMSprop(mdl.parameters(), lr=learning_rate)
    elif(optimizer_name == 'SGD'):
        optimizer = optim.SGD(mdl.parameters(), lr=learning_rate)
    elif(optimizer_name == 'Adagrad'):
        optimizer = optim.Adagrad(mdl.parameters(), lr=learning_rate)
    elif(optimizer_name == 'Adadelta'):
        optimizer = optim.Adadelta(mdl.parameters(), lr=learning_rate)
    else:
        RuntimeError("Optimizer '{}' is unknown!".format(optimizer_name))
    return optimizer


# def packed_rnn(rnn, inputs, hidden, lengths, use_cuda=False):
#     sorted_len, sorted_idx = lengths.sort(0, descending=True)
#     # sorted_encoded = inputs.gather(0, sorted_idx.view(-1, 1, 1).expand_as(inputs))
#     sorted_encoded = inputs[sorted_idx]
#     packed_encoded = torch.nn.utils.rnn.pack_padded_sequence(sorted_encoded,
#                                                              ungpu(sorted_len, use_cuda).numpy(),
#                                                              batch_first=True)

#     rnn_out, hidden = rnn(packed_encoded, hidden)  # rnn_out: batch_size x conv_len x hidden_dim

#     unpacked, unpacked_len = torch.nn.utils.rnn.pad_packed_sequence(rnn_out, batch_first=True)

#     _, unsorted_idx = sorted_idx.sort(0, descending=False)

#     outputs = unpacked[unsorted_idx]
#     padding = inputs.size(1) - outputs.size(1)
#     outputs = nn.functional.pad(outputs, (0, 0, 0, padding), value=0)

#     return outputs, hidden


def packed_rnn(rnn, encoded, lengths):
    sorted_len, sorted_idx = lengths.sort(0, descending=True)
    sorted_encoded = encoded[sorted_idx]
    packed_encoded = torch.nn.utils.rnn.pack_padded_sequence(sorted_encoded,
                                                             sorted_len,
                                                             batch_first=True)
    rnn_out, _ = rnn(packed_encoded)
    rnn_out, _ = torch.nn.utils.rnn.pad_packed_sequence(rnn_out, batch_first=True)
    idx = (sorted_len - 1).view(-1, 1).expand(rnn_out.size(0), rnn_out.size(2)).unsqueeze(1)
    decoded = rnn_out.gather(1, idx).squeeze()

    _, unsorted_idx = sorted_idx.sort(0, descending=False)
    decoded = decoded[unsorted_idx]

    return decoded


def fit_batch(model, batch_input, batch_output, optimizer, criterion,
              lengths=None, is_sequence=False):
    if not isinstance(criterion, collections.Sequence):
        criterion = [criterion] * len(batch_output)
    if not isinstance(is_sequence, collections.Sequence):
        is_sequence = [is_sequence] * len(batch_output)
    optimizer.zero_grad()
    if lengths is None:
        outputs, _ = model(batch_input)
    else:
        outputs, _ = model(batch_input, lengths)
    loss = None
    for i in range(len(outputs)):
        if is_sequence[i]:
            # Combine first two dimensions
            pred_size = (-1,) + tuple(outputs[i].size()[2:])
            truth_size = (-1,) + tuple(batch_output[i].size()[2:])
            pred = outputs[i].view(pred_size)
            truth = batch_output[i].view(truth_size)
        else:
            pred = outputs[i]
            truth = batch_output[i]

        if loss is None:
            loss = criterion[i](pred, truth)
        else:
            batch_loss = criterion[i](pred, truth)
            loss += batch_loss
    loss.backward()
    optimizer.step()

    return loss.item()


def fit_generator(model, generator, optimizer, criterion, n_batches: int,
                  is_sequence: bool=False, n_epochs: int=1,
                  valid_generator=None, n_valid_batches: int=None,
                  cuda: bool=False, checkpoint: bool=False, verbose: bool=False):
    """Fit a pytorch model using generators for the inputs.
    Generators must be infinite and must return (input, output, lengths=None)
    """
    model.train(True)
    best_loss = None
    best_epoch = 0
    for epoch in range(1, n_epochs + 1):
        start_time = time.time()
        avg_loss = 0.0
        n_samples = 0
        if verbose:
            pbar = tqdm(total=n_batches,
                        desc="Epoch {}".format(epoch),
                        unit="batch")
        for batch_idx, batch in enumerate(generator):
            batch_input = [numpy_to_tensors(x, cuda) for x in batch[0]]  # numpy_to_variable(batch[0], cuda)
            batch_output = [numpy_to_tensors(x, cuda) for x in batch[1]]  # numpy_to_variable(batch[1], cuda)
            batch_lengths = [numpy_to_tensors(x, cuda) for x in batch[2]] if len(batch) > 2 and batch[2] is not None else None
            n_batch_samples = len(batch_input[0])
            n_samples += n_batch_samples
            loss = fit_batch(model, batch_input, batch_output, optimizer, criterion, batch_lengths, is_sequence)
            avg_loss += loss * n_batch_samples
            if verbose:
                pbar.set_description("Epoch {} - Loss: {:.6}".format(epoch, avg_loss / n_samples))
                pbar.update(1)
            if batch_idx == n_batches - 1:
                break
        if verbose:
            pbar.close()
        end_time = time.time()
        avg_loss = avg_loss / n_samples
        cmp_loss = avg_loss
        if valid_generator is not None:
            valid_start_time = time.time()
            valid_loss = validate_generator(model, valid_generator, criterion,
                                            n_valid_batches, is_sequence, cuda=cuda)
            cmp_loss = valid_loss
            valid_end_time = time.time()
        if checkpoint is not None and (best_loss is None or cmp_loss < best_loss):
            best_loss = cmp_loss
            best_epoch = epoch
            torch.save(model, checkpoint)

        epoch_duration = end_time - start_time
        if valid_generator is None:
            print("[Epoch {}] Train Loss={} (Took {:.1f} seconds)".format(epoch, avg_loss, epoch_duration),
                  flush=True)
        else:
            valid_duration = valid_end_time - valid_start_time
            print("[Epoch {}] Train Loss={} - Valid Loss={} (Took {:.1f} + {:.1f} seconds)".format(epoch,
                                                                                                   avg_loss,
                                                                                                   valid_loss,
                                                                                                   epoch_duration,
                                                                                                   valid_duration),
                  flush=True)

    if checkpoint is not None:
        print("Best epoch: {} with Loss: {}".format(best_epoch, best_loss), flush=True)


def fit(model, train_input, train_output,
        optimizer, criterion,
        lengths=None, is_sequence=False,
        batch_size=32, n_epochs=1,
        shuffle=True, valid=None,
        cuda=False, checkpoint=False, verbose=True):
    """Fit a pytorch model.

    :param model: Pytorch nn.Module model instance.
    :param train_input: List of torch tensors to use as inputs.
    :param train_ouput: List of torch tensors to use as targets.
    :param optimizer: Optimizer instance to use (can be selected using select_optimizer).
    :param criterion: Criterion used to calculate the loss.
    """
    model.train(True)
    best_loss = None
    best_epoch = 0
    for epoch in range(1, n_epochs + 1):
        start_time = time.time()
        indices = None
        if shuffle:
            indices = shuffle_indices(len(train_input[0]))
        avg_loss = 0.0
        n_samples = 0
        if verbose:
            batch_range = tqdm(range(0, len(train_input[0]), batch_size),
                               desc="Epoch {}".format(epoch),
                               unit="batch")
        else:
            batch_range = range(0, len(train_input[0]), batch_size)
        for batch_start in batch_range:
            batch_input = make_batch(train_input, batch_size, batch_start, indices, cuda)
            batch_output = make_batch(train_output, batch_size, batch_start, indices, cuda)
            batch_lengths = None
            if lengths is not None:
                batch_lengths = make_lengths_batch(lengths, batch_size, batch_start, indices, cuda)
            n_samples += len(batch_input[0])
            loss = fit_batch(model, batch_input, batch_output, optimizer, criterion, batch_lengths, is_sequence)
            avg_loss += loss * len(batch_input[0])
            if verbose:
                batch_range.set_description("Epoch {} - Loss: {:.6}".format(epoch, avg_loss / n_samples))
        end_time = time.time()
        avg_loss = avg_loss / n_samples
        cmp_loss = avg_loss
        if valid is not None:
            valid_start_time = time.time()
            valid_lengths = None if len(valid) == 2 else valid[2]
            valid_loss = validate(model, valid[0], valid[1], criterion,
                                  valid_lengths, is_sequence,
                                  batch_size=batch_size, cuda=cuda)
            cmp_loss = valid_loss
            valid_end_time = time.time()
        if checkpoint is not None and (best_loss is None or cmp_loss < best_loss):
            best_loss = cmp_loss
            best_epoch = epoch
            torch.save(model, checkpoint)

        epoch_duration = end_time - start_time
        if valid is None:
            print("[Epoch {}] Train Loss={} (Took {:.1f} seconds)".format(epoch, avg_loss, epoch_duration),
                  flush=True)
        else:
            valid_duration = valid_end_time - valid_start_time
            print("[Epoch {}] Train Loss={} - Valid Loss={} (Took {:.1f} + {:.1f} seconds)".format(epoch,
                                                                                                   avg_loss,
                                                                                                   valid_loss,
                                                                                                   epoch_duration,
                                                                                                   valid_duration),
                  flush=True)

    if checkpoint is not None:
        print("Best epoch: {} with Loss: {}".format(best_epoch, best_loss), flush=True)


def fit_batch_no_loss(model, batch_input, batch_output, optimizer, lengths=None):
    optimizer.zero_grad()
    if lengths is None:
        loss, outputs, _ = model(batch_input, batch_output)
    else:
        loss, outputs, _ = model(batch_input, batch_output, lengths)
    loss.backward()
    optimizer.step()

    return loss.item()


def fit_no_loss(model, train_input, train_output, optimizer,
                lengths=None, batch_size=32, n_epochs=1,
                shuffle=True, valid=None,
                cuda=False, checkpoint=False, verbose=True):
    """Fit a pytorch model which directly returns the loss.

    :param model: Pytorch nn.Module model instance.
    :param train_input: List of torch tensors to use as inputs.
    :param train_ouput: List of torch tensors to use as targets.
    :param optimizer: Optimizer instance to use (can be selected using select_optimizer).
    """
    model.train(True)
    best_loss = None
    best_epoch = 0
    for epoch in range(1, n_epochs + 1):
        start_time = time.time()
        indices = None
        if shuffle:
            indices = shuffle_indices(len(train_input[0]))
        avg_loss = 0.0
        n_samples = 0
        if verbose:
            batch_range = tqdm(range(0, len(train_input[0]), batch_size),
                               desc="Epoch {}".format(epoch),
                               unit="batch")
        else:
            batch_range = range(0, len(train_input[0]), batch_size)
        for batch_start in batch_range:
            batch_input = make_batch(train_input, batch_size, batch_start, indices, cuda)
            batch_output = make_batch(train_output, batch_size, batch_start, indices, cuda)
            batch_lengths = None
            if lengths is not None:
                batch_lengths = make_lengths_batch(lengths, batch_size, batch_start, indices, cuda)
            n_samples += len(batch_input[0])
            loss = fit_batch_no_loss(model, batch_input, batch_output, optimizer, batch_lengths)
            avg_loss += loss * len(batch_input[0])
            if verbose:
                batch_range.set_description("Epoch {} - Loss: {:.6}".format(epoch, avg_loss / n_samples))
        end_time = time.time()
        avg_loss = avg_loss / n_samples
        cmp_loss = avg_loss
        if valid is not None:
            valid_start_time = time.time()
            valid_lengths = None if len(valid) == 2 else valid[2]
            valid_loss = validate_no_loss(model, valid[0], valid[1], valid_lengths,
                                          batch_size=batch_size, cuda=cuda)
            cmp_loss = valid_loss
            valid_end_time = time.time()
        if checkpoint is not None and (best_loss is None or cmp_loss < best_loss):
            best_loss = cmp_loss
            best_epoch = epoch
            torch.save(model, checkpoint)

        epoch_duration = end_time - start_time
        if valid is None:
            print("[Epoch {}] Train Loss={} (Took {:.1f} seconds)".format(epoch, avg_loss, epoch_duration),
                  flush=True)
        else:
            valid_duration = valid_end_time - valid_start_time
            print("[Epoch {}] Train Loss={} - Valid Loss={} (Took {:.1f} + {:.1f} seconds)".format(epoch,
                                                                                                   avg_loss,
                                                                                                   valid_loss,
                                                                                                   epoch_duration,
                                                                                                   valid_duration),
                  flush=True)

    if checkpoint is not None:
        print("Best epoch: {} with Loss: {}".format(best_epoch, best_loss), flush=True)


def fit_seq2seq(model, train_input, train_output,
                optimizer, criterion,
                n_epochs=1, shuffle=True, valid=None,
                cuda=False, checkpoint=False, verbose=True):
    """Fit a pytorch seq2seq-like model.

    :param model: Pytorch nn.Module model instance.
    :param train_input: List of list of torch tensors to use as inputs (shape).
    :param train_ouput: List of list of torch tensors to use as targets.
    :param optimizer: Optimizer instance to use (can be selected using select_optimizer).
    :param criterion: Criterion used to calculate the loss.

    .. note:: Model must must have two modes. One with only_loss=True where forward returns the loss history
    and the loss value. One with only_loss=False where it returns the usual predictions output tuple.
    """
    model.train(True)
    best_loss = None
    best_epoch = 0
    indices = np.arange(len(train_input[0]))
    for epoch in range(1, n_epochs + 1):
        start_time = time.time()
        if shuffle:
            indices = np.random.permutation(indices)
        avg_loss = 0.0
        n_samples = 0
        sample_range = tqdm(range(0, len(train_input[0])),
                            desc="Epoch {}".format(epoch),
                            unit=" sample", disable=not verbose)
        for sample_idx in sample_range:
            optimizer.zero_grad()
            hist, loss = model([gpu(feat[indices[sample_idx]], cuda) for feat in train_input],
                               [gpu(target[indices[sample_idx]], cuda) for target in train_output],
                               only_loss=True)
            avg_loss += loss
            hist.backward()
            optimizer.step()

            sample_range.set_description("Epoch {} - Loss: {:.6}".format(epoch, avg_loss / n_samples))
        end_time = time.time()
        avg_loss = avg_loss / n_samples
        cmp_loss = avg_loss
        if valid is not None:
            valid_start_time = time.time()
            valid_loss = validate_seq2seq(model, valid[0], valid[1], criterion, cuda=cuda)
            cmp_loss = valid_loss
            valid_end_time = time.time()
        if checkpoint is not None and (best_loss is None or cmp_loss < best_loss):
            best_loss = cmp_loss
            best_epoch = epoch
            torch.save(model, checkpoint)

        epoch_duration = end_time - start_time
        if valid is None:
            print("[Epoch {}] Train Loss={} (Took {:.1f} seconds)".format(epoch, avg_loss, epoch_duration),
                  flush=True)
        else:
            valid_duration = valid_end_time - valid_start_time
            print("[Epoch {}] Train Loss={} - Valid Loss={} (Took {:.1f} + {:.1f} seconds)".format(epoch,
                                                                                                   avg_loss,
                                                                                                   valid_loss,
                                                                                                   epoch_duration,
                                                                                                   valid_duration),
                  flush=True)

    if checkpoint is not None:
        print("Best epoch: {} with Loss: {}".format(best_epoch, best_loss), flush=True)


def fit_seq2seq_generator(model, generator, n_samples,
                          optimizer, criterion,
                          n_epochs=1, valid_generator=None,
                          n_valid_samples=None,
                          cuda=False, checkpoint=None, verbose=True):
    """Fit a pytorch seq2seq-like model.

    :param model: Pytorch nn.Module model instance.
    :param generator: Python generator that yields a couple with an array of inputs and an array of outputs.
    :param n_samples: Number of samples to yield per epoch.
    :param optimizer: Optimizer instance to use (can be selected using select_optimizer).
    :param criterion: Criterion used to calculate the loss.

    .. note:: Model must must have two modes. One with only_loss=True where forward returns the loss history
    and the loss value. One with only_loss=False where it returns the usual predictions output tuple.
    """
    model.train(True)
    best_loss = None
    best_epoch = 0
    for epoch in range(1, n_epochs + 1):
        start_time = time.time()
        avg_loss = 0.0
        pbar = tqdm(total=n_samples,
                    desc="Epoch {}".format(epoch),
                    unit=" sample", disable=not verbose)
        for sample_idx, sample_sequence in enumerate(generator):
            sample_input = [numpy_to_tensors(feat, cuda) for feat in sample_sequence[0]]
            output_idx = 1
            sample_lengths = None
            if len(sample_sequence) > 2:
                sample_lengths = [numpy_to_tensors(length, cuda) for length in sample_sequence[1]]
                output_idx = 2
            sample_output = [numpy_to_tensors(target, cuda) for target in sample_sequence[output_idx]]
            optimizer.zero_grad()
            hist, loss = model(sample_input, sample_lengths, sample_output, only_loss=True)
            hist.backward()
            optimizer.step()
            avg_loss += loss
            pbar.set_description("Epoch {} - Loss: {:.6}".format(epoch, avg_loss / (sample_idx + 1)))
            pbar.update(1)
            if sample_idx + 1 == n_samples:
                break
        pbar.close()
        end_time = time.time()
        avg_loss = avg_loss / n_samples
        cmp_loss = avg_loss
        if valid_generator is not None:
            valid_start_time = time.time()
            valid_loss = validate_seq2seq_generator(model, valid_generator, n_valid_samples,
                                                    criterion, cuda=cuda)
            cmp_loss = valid_loss
            valid_end_time = time.time()
        if checkpoint is not None and (best_loss is None or cmp_loss < best_loss):
            best_loss = cmp_loss
            best_epoch = epoch
            torch.save(model, checkpoint)

        epoch_duration = end_time - start_time
        if valid_generator is None:
            print("[Epoch {}] Train Loss={} (Took {:.1f} seconds)".format(epoch, avg_loss, epoch_duration),
                  flush=True)
        else:
            valid_duration = valid_end_time - valid_start_time
            print("[Epoch {}] Train Loss={} - Valid Loss={} (Took {:.1f} + {:.1f} seconds)".format(epoch,
                                                                                                   avg_loss,
                                                                                                   valid_loss,
                                                                                                   epoch_duration,
                                                                                                   valid_duration),
                  flush=True)

    if checkpoint is not None:
        print("Best epoch: {} with Loss: {}".format(best_epoch, best_loss), flush=True)


@torch.no_grad()
def validate_batch(model, batch_input, batch_output, criterion, lengths=None, is_sequence=False):
    if not isinstance(criterion, collections.Sequence):
        criterion = [criterion] * len(batch_output)
    if not isinstance(is_sequence, collections.Sequence):
        is_sequence = [is_sequence] * len(batch_output)
    if lengths is None:
        outputs, _ = model(batch_input)
    else:
        outputs, _ = model(batch_input, lengths)
    loss = None
    for i in range(len(outputs)):
        if is_sequence[i]:
            # Combine first two dimensions
            pred_size = (-1,) + tuple(outputs[i].size()[2:])
            truth_size = (-1,) + tuple(batch_output[i].size()[2:])
            pred = outputs[i].view(pred_size)
            truth = batch_output[i].view(truth_size)
        else:
            pred = outputs[i]
            truth = batch_output[i]
        if loss is None:
            loss = criterion[i](pred, truth)
        else:
            loss += criterion[i](pred, truth)
    return outputs, loss.item()


@torch.no_grad()
def validate_generator(model, generator, criterion, n_batches,
                       is_sequence=False, cuda=False):
    model.eval()
    avg_loss = 0.0
    n_samples = 0
    for batch_idx, batch in enumerate(generator):
        batch_input = numpy_to_variable(batch[0], cuda=cuda)
        batch_output = numpy_to_variable(batch[1], cuda=cuda)
        batch_lengths = [numpy_to_tensors(x, cuda) for x in batch[2]] if len(batch) > 2 and batch[2] is not None else None
        n_batch_samples = len(batch_input[0])
        n_samples += n_batch_samples
        _, loss = validate_batch(model, batch_input, batch_output, criterion, batch_lengths, is_sequence)
        avg_loss += loss * n_batch_samples
        if batch_idx == n_batches - 1:
            break
    avg_loss = avg_loss / n_samples
    model.train()
    return avg_loss


@torch.no_grad()
def validate(model, valid_input, valid_output, criterion,
             lengths=None, is_sequence=False,
             batch_size=32, cuda=False):
    model.eval()
    avg_loss = 0.0
    n_samples = 0
    batch_range = range(0, len(valid_input[0]), batch_size)
    for batch_start in batch_range:
        batch_input = make_batch(valid_input, batch_size, batch_start, cuda=cuda)
        batch_output = make_batch(valid_output, batch_size, batch_start, cuda=cuda)
        batch_lengths = None
        if lengths is not None:
            batch_lengths = make_lengths_batch(lengths, batch_size, batch_start, cuda=cuda)
        n_samples += len(batch_input[0])
        _, loss = validate_batch(model, batch_input, batch_output, criterion, batch_lengths, is_sequence)
        avg_loss += loss * len(batch_input[0])
    avg_loss = avg_loss / n_samples
    model.train()
    return avg_loss


@torch.no_grad()
def validate_batch_no_loss(model, batch_input, batch_output, lengths=None):
    if lengths is None:
        loss, outputs, _ = model(batch_input)
    else:
        loss, outputs, _ = model(batch_input, lengths)
    return outputs, loss.item()


@torch.no_grad()
def validate_no_loss(model, valid_input, valid_output, lengths=None,
                     batch_size=32, cuda=False):
    model.eval()
    avg_loss = 0.0
    n_samples = 0
    batch_range = range(0, len(valid_input[0]), batch_size)
    for batch_start in batch_range:
        batch_input = make_batch(valid_input, batch_size, batch_start, cuda=cuda)
        batch_output = make_batch(valid_output, batch_size, batch_start, cuda=cuda)
        batch_lengths = None
        if lengths is not None:
            batch_lengths = make_lengths_batch(lengths, batch_size, batch_start, cuda=cuda)
        n_samples += len(batch_input[0])
        _, loss = validate_batch_no_loss(model, batch_input, batch_output, batch_lengths)
        avg_loss += loss * len(batch_input[0])
    avg_loss = avg_loss / n_samples
    model.train()
    return avg_loss


@torch.no_grad()
def validate_seq2seq(model, valid_input, valid_output, criterion, cuda=False):
    model.eval()
    avg_loss = 0.0
    sample_range = range(0, len(valid_input[0]))
    for sample_idx in sample_range:
        _, loss = model([gpu(feat[sample_idx], cuda) for feat in valid_input],
                        [gpu(target[sample_idx], cuda) for target in valid_output],
                        only_loss=True)
        avg_loss += loss
    avg_loss = avg_loss / len(valid_input[0])
    model.train()
    return avg_loss


@torch.no_grad()
def validate_seq2seq_generator(model, generator, n_samples, criterion, cuda=False):
    model.eval()
    avg_loss = 0.0
    for sample_idx, sample_sequence in enumerate(generator):
        sample_input = [numpy_to_tensors(feat, cuda) for feat in sample_sequence[0]]
        output_idx = 1
        sample_lengths = None
        if len(sample_sequence) > 2:
            sample_lengths = [numpy_to_tensors(length, cuda) for length in sample_sequence[1]]
            output_idx = 2
        sample_output = [numpy_to_tensors(target, cuda) for target in sample_sequence[output_idx]]
        _, loss = model(sample_input, sample_lengths, sample_output, only_loss=True)
        avg_loss += loss
        if sample_idx + 1 == n_samples:
            break
    avg_loss = avg_loss / n_samples
    model.train()
    return avg_loss


@torch.no_grad()
def predict_batch(model, batch_input, batch_output=None, criterion=None, lengths=None, is_sequence=False):
    if lengths is None:
        outputs, hidden = model(batch_input)
    else:
        outputs, hidden = model(batch_input, lengths)

    if batch_output is None:
        return outputs, 0, hidden

    if not isinstance(criterion, collections.Sequence):
        criterion = [criterion] * len(batch_output)
    if not isinstance(is_sequence, collections.Sequence):
        is_sequence = [is_sequence] * len(batch_output)

    loss = None
    for i in range(len(outputs)):
        if is_sequence[i]:
            # Combine first two dimensions
            pred_size = (-1,) + tuple(outputs[i].size()[2:])
            truth_size = (-1,) + tuple(batch_output[i].size()[2:])
            pred = outputs[i].view(pred_size)
            truth = batch_output[i].view(truth_size)
        else:
            pred = outputs[i]
            truth = batch_output[i]
        if loss is None:
            loss = criterion[i](pred, truth)
        else:
            loss += criterion[i](pred, truth)
    return outputs, loss.item(), hidden


@torch.no_grad()
def predict_generator(model, generator, n_batches, n_total_samples,
                      criterion=None,
                      is_sequence=False,
                      cuda=False,
                      pad_value=-1,
                      return_hiddens=False,
                      verbose=False):
    model.eval()
    avg_loss = 0.0
    n_samples = 0
    test_preds = None
    if return_hiddens:
        hiddens = []
    pbar = tqdm(total=n_batches, desc="Predicting", unit="batch", disable=not verbose)
    batch_start = 0
    test_preds = []
    for batch_idx, batch in enumerate(generator):
        batch_input = numpy_to_variable(batch[0], cuda=cuda)
        batch_output = None
        if criterion is not None:
            batch_output = numpy_to_variable(batch[1], cuda=cuda)
        len_idx = 2 if criterion is not None else 1
        batch_lengths = [numpy_to_tensors(x, cuda) for x in batch[len_idx]] if len(batch) > len_idx and batch[len_idx] is not None else None
        n_batch_samples = len(batch_input[0])
        n_samples += n_batch_samples
        batch_preds, loss, hidden = predict_batch(model, batch_input, batch_output, criterion,
                                                  batch_lengths, is_sequence)
        avg_loss += loss * n_batch_samples
        for output_idx, batch_pred in enumerate(batch_preds):
            batch_pred = ungpu(batch_pred, cuda)
            if len(test_preds) <= output_idx:
                shape = tuple([n_total_samples] + list(batch_pred.size()[1:]))
                test_preds.append(torch.full(shape, pad_value, dtype=batch_pred.dtype))
            test_preds[output_idx][batch_start:n_batch_samples + batch_start] = batch_pred
        if return_hiddens:
            for hidden_idx, hidden_out in enumerate(hidden):
                if hidden_out is None:
                    continue
                hidden_out = ungpu(hidden_out, cuda)
                if hidden_idx >= len(hiddens):
                    shape = tuple([n_total_samples] + list(hidden_out.size()[1:]))
                    hiddens.append(torch.zeros(shape, dtype=hidden_out.dtype))
                hiddens[hidden_idx][batch_start:n_batch_samples + batch_start] = hidden_out
        pbar.update(1)
        if batch_idx == n_batches - 1:
            break
        batch_start += n_batch_samples
    pbar.close()
    avg_loss = avg_loss / n_samples

    if return_hiddens:
        return test_preds, avg_loss, hiddens
    return test_preds, avg_loss


@torch.no_grad()
def predict_generator_list(model, generator, n_batches, n_total_samples,
                           criterion=None,
                           is_sequence=False,
                           cuda=False,
                           pad_value=-1,
                           return_hiddens=False,
                           verbose=False):
    model.eval()
    avg_loss = 0.0
    n_samples = 0
    test_preds = None
    if return_hiddens:
        hiddens = []
    pbar = tqdm(total=n_batches, desc="Predicting", unit="batch", disable=not verbose)
    batch_start = 0
    test_preds = []
    for batch_idx, batch in enumerate(generator):
        batch_input = numpy_to_variable(batch[0], cuda=cuda)
        batch_output = None
        if criterion is not None:
            batch_output = numpy_to_variable(batch[1], cuda=cuda)
        len_idx = 2 if criterion is not None else 1
        batch_lengths = [numpy_to_tensors(x, cuda) for x in batch[len_idx]] if len(batch) > len_idx and batch[len_idx] is not None else None
        n_batch_samples = len(batch_input[0])
        n_samples += n_batch_samples
        batch_preds, loss, hidden = predict_batch(model, batch_input, batch_output, criterion,
                                                  batch_lengths, is_sequence)
        avg_loss += loss * n_batch_samples
        for output_idx, batch_pred in enumerate(batch_preds):
            if len(test_preds) <= output_idx:
                test_preds.append([])
            batch_pred = ungpu(batch_pred, cuda)
            test_preds[output_idx].append(batch_pred)
        if return_hiddens:
            for hidden_idx, hidden_out in enumerate(hidden):
                if hidden_out is None:
                    continue
                hidden_out = ungpu(hidden_out, cuda)
                if hidden_idx >= len(hiddens):
                    shape = tuple([n_total_samples] + list(hidden_out.size()[1:]))
                    hiddens.append(torch.zeros(shape, dtype=hidden_out.dtype))
                hiddens[hidden_idx][batch_start:n_batch_samples + batch_start] = hidden_out
        pbar.update(1)
        if batch_idx == n_batches - 1:
            break
        batch_start += n_batch_samples
    pbar.close()
    avg_loss = avg_loss / n_samples

    if return_hiddens:
        return test_preds, avg_loss, hiddens
    return test_preds, avg_loss


@torch.no_grad()
def predict(model, test_input, test_output=None, criterion=None, lengths=None, is_sequence=False,
            batch_size=1, cuda=False, return_hiddens=False,
            verbose=False):
    model.eval()
    avg_loss = 0.0
    n_samples = 0
    n_total_samples = len(test_output[0])
    test_preds = [None] * len(test_output)
    if return_hiddens:
        hiddens = []
    if verbose:
        batch_range = tqdm(range(0, len(test_input[0]), batch_size),
                           desc="Predicting",
                           unit="batch")
    else:
        batch_range = range(0, len(test_input[0]), batch_size)
    for batch_start in batch_range:
        batch_input = make_batch(test_input, batch_size, batch_start, cuda=cuda)
        batch_output = None
        if test_output is not None:
            batch_output = make_batch(test_output, batch_size, batch_start, cuda=cuda)
        batch_lengths = None
        if lengths is not None:
            batch_lengths = make_lengths_batch(lengths, batch_size, batch_start, cuda=cuda)
        n_samples += len(batch_input[0])
        batch_preds, loss, hidden = predict_batch(model, batch_input, batch_output, criterion,
                                                  batch_lengths, is_sequence)
        avg_loss += loss * len(batch_input[0])
        for output_idx, batch_pred in enumerate(batch_preds):
            batch_pred = ungpu(batch_pred, cuda)
            if test_preds[output_idx] is None:
                # test_preds[output_idx] = batch_pred
                shape = tuple([n_total_samples] + list(batch_pred.size()[1:]))
                test_preds[output_idx] = torch.empty(shape, dtype=batch_pred.dtype)
            # else:
            #     test_preds[output_idx] = torch.cat((test_preds[output_idx], batch_pred), 0)
            test_preds[output_idx][batch_start:batch_size + batch_start] = batch_pred
        if return_hiddens:
            for hidden_idx, hidden_out in enumerate(hidden):
                if hidden_out is None:
                    continue
                hidden_out = ungpu(hidden_out, cuda)
                if hidden_idx >= len(hiddens):
                    # hiddens.append(hidden_out)
                    shape = tuple([n_total_samples] + list(hidden_out.size()[1:]))
                    hiddens.append(torch.empty(shape, dtype=hidden_out.dtype))
                # else:
                #     hiddens[hidden_idx] = torch.cat((hiddens[hidden_idx], hidden_out), 0)
                hiddens[hidden_idx][batch_start:batch_size + batch_start] = hidden_out
    avg_loss = avg_loss / n_samples
    if return_hiddens:
        return test_preds, avg_loss, hiddens
    return test_preds, avg_loss


@torch.no_grad()
def predict_batch_no_loss(model, batch_input, batch_output=None, lengths=None):
    if lengths is None:
        loss, outputs, hidden = model(batch_input, batch_output)
    else:
        loss, outputs, hidden = model(batch_input, batch_output, lengths)

    if batch_output is not None:
        return outputs, loss.item(), hidden
    return outputs, 0, hidden


@torch.no_grad()
def predict_no_loss(model, test_input, test_output=None, lengths=None,
                    batch_size=1, cuda=False, return_hiddens=False,
                    verbose=False):
    model.eval()
    avg_loss = 0.0
    n_samples = 0
    n_total_samples = len(test_output[0])
    test_preds = [None] * len(test_output)
    if return_hiddens:
        hiddens = []
    if verbose:
        batch_range = tqdm(range(0, len(test_input[0]), batch_size),
                           desc="Predicting",
                           unit="batch")
    else:
        batch_range = range(0, len(test_input[0]), batch_size)
    for batch_start in batch_range:
        batch_input = make_batch(test_input, batch_size, batch_start, cuda=cuda)
        batch_output = None
        if test_output is not None:
            batch_output = make_batch(test_output, batch_size, batch_start, cuda=cuda)
        batch_lengths = None
        if lengths is not None:
            batch_lengths = make_lengths_batch(lengths, batch_size, batch_start, cuda=cuda)
        n_samples += len(batch_input[0])
        batch_preds, loss, hidden = predict_batch_no_loss(model, batch_input, batch_output, batch_lengths)
        avg_loss += loss * len(batch_input[0])
        for output_idx, batch_pred in enumerate(batch_preds):
            batch_pred = ungpu(batch_pred, cuda)
            if test_preds[output_idx] is None:
                shape = tuple([n_total_samples] + list(batch_pred.size()[1:]))
                test_preds[output_idx] = torch.empty(shape, dtype=batch_pred.dtype)
            test_preds[output_idx][batch_start:batch_size + batch_start] = batch_pred
        if return_hiddens:
            for hidden_idx, hidden_out in enumerate(hidden):
                if hidden_out is None:
                    continue
                hidden_out = ungpu(hidden_out, cuda)
                if hidden_idx >= len(hiddens):
                    shape = tuple([n_total_samples] + list(hidden_out.size()[1:]))
                    hiddens.append(torch.empty(shape, dtype=hidden_out.dtype))
                hiddens[hidden_idx][batch_start:batch_size + batch_start] = hidden_out
    avg_loss = avg_loss / n_samples
    if return_hiddens:
        return test_preds, avg_loss, hiddens
    return test_preds, avg_loss


@torch.no_grad()
def predict_seq2seq(model, test_input, test_output=None,
                    cuda=False, return_hiddens=False, verbose=False):
    model.eval()
    n_total_samples = len(test_input[0])
    if return_hiddens:
        hiddens = []
    sample_range = tqdm(range(0, len(test_input[0])),
                        desc="Predicting",
                        unit=" sample",
                        disable=not verbose)
    predictions = []
    sum_loss = 0
    for sample_idx in sample_range:
        if test_output is None:
            sample_preds, hidden = model([gpu(feat[sample_idx], cuda) for feat in test_input], only_loss=False)
        else:
            sample_preds, hidden, loss = model([gpu(feat[sample_idx], cuda) for feat in test_input],
                                               None,
                                               [gpu(target[sample_idx], cuda) for target in test_output],
                                               only_loss=False)
            sum_loss += loss
        for output_idx, sample_pred in enumerate(sample_preds):
            if output_idx >= len(predictions):
                predictions.append([])
            predictions[output_idx].append(sample_pred)
        if return_hiddens:
            for hidden_idx, hidden_out in enumerate(hidden):
                if hidden_out is None:
                    continue
                hidden_out = ungpu(hidden_out, cuda)
                if hidden_idx >= len(hiddens):
                    shape = tuple([n_total_samples] + list(hidden_out.size()[1:]))
                    hiddens.append(torch.empty(shape, dtype=hidden_out.dtype))
                hiddens[hidden_idx][sample_idx] = hidden_out
    output = (predictions, )
    if return_hiddens:
        output += (hiddens, )
    if test_output is not None:
        output += (sum_loss / len(test_input), )
    return output[0] if len(output) == 1 else output


@torch.no_grad()
def predict_seq2seq_generator(model, generator, n_samples, return_loss=False,
                              cuda=False, return_hiddens=False, verbose=False):
    model.eval()
    if return_hiddens:
        hiddens = []
    pbar = tqdm(total=n_samples,
                desc="Predicting",
                unit=" sample",
                disable=not verbose)
    predictions = []
    sum_loss = 0
    for sample_idx, sample_sequence in enumerate(generator):
        if not return_loss:
            sample_input = [numpy_to_tensors(feat, cuda) for feat in sample_sequence[0]]
            sample_lengths = None
            if len(sample_sequence) > 1:
                sample_lengths = [numpy_to_tensors(length, cuda) for length in sample_sequence[1]]
            sample_output = None
            sample_preds, hidden = model(sample_input, sample_lengths, sample_output, only_loss=False)
        else:
            sample_input = [numpy_to_tensors(feat, cuda) for feat in sample_sequence[0]]
            output_idx = 1
            sample_lengths = None
            if len(sample_sequence) > 2:
                sample_lengths = [numpy_to_tensors(length, cuda) for length in sample_sequence[1]]
                output_idx = 2
            sample_output = [numpy_to_tensors(target, cuda) for target in sample_sequence[output_idx]]
            sample_preds, hidden, loss = model(sample_input, sample_lengths, sample_output, only_loss=False)
            sum_loss += loss
        for output_idx, sample_pred in enumerate(sample_preds):
            if output_idx >= len(predictions):
                predictions.append([])
            predictions[output_idx].append(sample_pred)
        if return_hiddens:
            for hidden_idx, hidden_out in enumerate(hidden):
                if hidden_out is None:
                    continue
                hidden_out = ungpu(hidden_out, cuda)
                if hidden_idx >= len(hiddens):
                    shape = tuple([n_samples] + list(hidden_out.size()[1:]))
                    hiddens.append(torch.empty(shape, dtype=hidden_out.dtype))
                hiddens[hidden_idx][sample_idx] = hidden_out
        pbar.update(1)
        if sample_idx + 1 == n_samples:
            break
    pbar.close()

    output = (predictions, )
    if return_hiddens:
        output += (hiddens, )
    if return_loss:
        output += (sum_loss / n_samples, )
    return output[0] if len(output) == 1 else output
