
class EmptyContext:
    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass


class Anon:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
