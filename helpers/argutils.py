import yaml
import logging
import argparse
from enum import Enum


class NegateAction(argparse.Action):
    def __call__(self, parser, ns, values, option):
        setattr(ns, self.dest, not option.startswith('--no-'))


class EnumAction(argparse.Action):
    """
    Argparse action for handling Enums
    """
    def __init__(self, **kwargs):
        # Pop off the type value
        enum = kwargs.pop("type", None)

        # Ensure an Enum subclass is provided
        if enum is None:
            raise ValueError("type must be assigned an Enum when using EnumAction")
        if not issubclass(enum, Enum):
            raise TypeError("type must be an Enum when using EnumAction")

        # Generate choices from the Enum
        kwargs.setdefault("choices", tuple(e.value for e in enum))

        super(EnumAction, self).__init__(**kwargs)

        self._enum = enum

    def __call__(self, parser, namespace, values, option_string=None):
        # Convert value back into an Enum
        enum = self._enum(values)
        setattr(namespace, self.dest, enum)


def load_args_yaml(args, config_filepath, warn_nonexistent=True):
    # TODO: Add type checking to make sure types are the same as specified.
    with open(config_filepath, 'r') as yaml_in:
        config = yaml.load(yaml_in)

    for argument, value in config.items():
        argument = argument.replace('-', '_')
        if warn_nonexistent and not hasattr(args, argument):
            logging.warning(f"Argument '{argument}' is undefined by the argument parser!")
        setattr(args, argument, value)


def print_args(args, use_logger=False):
    logger = print if not use_logger else logging.info

    for argument in vars(args):
        if getattr(args, argument) is None:
            continue
        logger("{}: {}".format(argument, getattr(args, argument)))
