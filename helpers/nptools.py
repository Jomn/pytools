import numpy as np
import tqdm


def merge_arrays(arrays, max_len=None, pad_value=0, cut_end=True, verbose=False):
    """ Merge arrays into a single array by adding an extra dimension and adding padding when necessary.
    """
    concat = None
    iterator = tqdm.tqdm(arrays, desc="Merging arrays") if verbose else arrays
    for dataset in iterator:
        if concat is None:
            if max_len is not None:
                start = max(0, len(dataset) - max_len)
                dataset = dataset[:max_len] if cut_end else dataset[start:]
            concat = np.expand_dims(dataset, 0)
            continue
        lengths = []
        for dim in range(len(dataset.shape)):
            if dim == 0 and max_len is not None:
                length = min(max(dataset.shape[dim], concat.shape[dim + 1]), max_len)
            else:
                length = max(dataset.shape[dim], concat.shape[dim + 1])
            lengths.append(length)
        pad_shapes = []
        for dim, length in enumerate(lengths):
            diff = max(length - dataset.shape[dim], 0)
            pad_shapes.append((0, diff))
        padded = np.pad(dataset, pad_shapes, 'constant', constant_values=pad_value)
        pad_shapes = [(0, 0)]
        for dim, length in enumerate(lengths):
            diff = max(length - concat.shape[dim + 1], 0)
            pad_shapes.append((0, diff))
        concat = np.pad(concat, pad_shapes, 'constant', constant_values=pad_value)
        if max_len is not None:
            start = max(0, len(padded) - max_len)
            padded = padded[:max_len] if cut_end else padded[start:]
        concat = np.concatenate((concat, np.expand_dims(padded, 0)), axis=0)
    return concat


def concat_arrays(arrays, pad_value=0):
    """ Concatenate arrays along the first dimension and add padding when necessary.
    """
    concat = None
    for array in arrays:
        if concat is None:
            concat = array
            continue
        lengths = []
        for dim in range(1, len(array.shape)):
            length = max(array.shape[dim], concat.shape[dim])
            lengths.append(length)
        pad_shapes = [(0,0)]
        for dim, length in enumerate(lengths, 1):
            diff = max(length - array.shape[dim], 0)
            pad_shapes.append((0, diff))
        padded = np.pad(array, pad_shapes, 'constant', constant_values=pad_value)
        pad_shapes = [(0,0)]
        for dim, length in enumerate(lengths, 1):
            diff = max(length - concat.shape[dim], 0)
            pad_shapes.append((0, diff))
        concat = np.pad(concat, pad_shapes, 'constant', constant_values=pad_value)
        concat = np.concatenate((concat, padded), axis=0)
    return concat
