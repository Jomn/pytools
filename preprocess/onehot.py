# coding: utf-8
""" Onehot conversion tools """

import numpy as np


def np_index_to_onehot(array, maxvalue=None):
    """ Transforms a 1-D numpy array with label indexes into a 2-D numpy array with one hot vectors.
    Args:
      array: A 1-D numpy array with values between 0 and maxvalue
      maxvalue: Maximum value that a label index can have. If None, it will be automatically calculated.

    Returns a 2-D array with label one hots.
    """
    if maxvalue is None:
        maxvalue = np.amax(array)
    onehots = np.zeros(array.shape + (maxvalue,), dtype='int32')
    onehots[np.arange(array.shape[0]), array] = 1
    return onehots


def index_to_onehot(array, maxvalue=None):
    if maxvalue is None:
        maxvalue = max(array)
    onehot_array = []
    for value in array:
        onehot_array.append([0] * (maxvalue+1))
        onehot_array[-1][value] = 1

    return onehot_array


def np_proba_to_index(array):
    return np.apply_along_axis(np.argmax, -1, array)


def onehot_to_index(array):
    pass
