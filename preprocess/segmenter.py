# coding: utf-8


def segment_delimiters(array, delimiter=None):
    """ Segments an array using a delimiter to split the array.
    Args:
      array (list): Input array to segment
      delimiter: Any value that will indicate where the splits have to be done.
    Returns: A list of lists where the lists are the new segments.
    """
    seg_array = []
    start_seg = 0
    for curr_idx, value in enumerate(array):
        if value == delimiter:
            if curr_idx - start_seg > 0:
                seg_array.append(seg_array[start_seg:curr_idx])
                start_seg = curr_idx + 1
            continue
    return seg_array


def flatten_delimiters(seg_array, delimiter=None):
    pass


def segment_by_length(array, length, padval=None):
    """ Segments an array into segments of equal length.
    Args:
      array (list): Input array to segment.
      length (int): Length of the new segments.
      padval: Pad value to use for the final segment if needed.
    """
    seg_array = [array[start:start+length] for start in range(0, len(array), length)]
    if len(seg_array[-1]) < length:
        seg_array[-1].extend([padval] * (length - len(seg_array[-1])))
    return seg_array


def flatten(seg_array, remove_pad=True, padval=None):
    pass


def segment_with_info(array, info_array, segment_value):
    """ Segments an array into segments according to another info array.
    Args:
      array (list): Input array to segment.
      info_array (list): Array of the same length which contains a segment value when
                         a split has to be done.
      segment_value: Segment value to use to identify splits.
    """
    seg_array = []
    start = 0
    for curr_idx, info in enumerate(info_array):
        if info == segment_value:
            seg_array.append(array[start:curr_idx+1])
            start = curr_idx + 1

    return seg_array
