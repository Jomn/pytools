from collections import defaultdict, Counter
import logging
import copy
import re
import typing
import tqdm


def create_vocabulary(dsv, column: str) -> typing.Counter:
    """Read text and return dictionary that encodes vocabulary
    """
    vocab = Counter()
    for row in dsv.read(field_filter=[column]):
        for word in row[column].strip('\r\n ').split(' '):
            if not word:
                continue
            vocab[word] += 1
    return vocab


def get_pair_statistics(vocab):
    """Count frequency of all symbol pairs, and create index"""

    # data structure of pair frequencies
    stats = defaultdict(int)

    # index from pairs to words
    indices = defaultdict(lambda: defaultdict(int))

    for i, (word, freq) in enumerate(vocab):
        prev_char = word[0]
        for char in word[1:]:
            stats[prev_char, char] += freq
            indices[prev_char, char][i] += 1
            prev_char = char

    return stats, indices


def prune_stats(stats, big_stats, threshold):
    """Prune statistics dict for efficiency of max()

    The frequency of a symbol pair never increases, so pruning is generally safe
    (until we the most frequent pair is less frequent than a pair we previously pruned)
    big_stats keeps full statistics for when we need to access pruned items
    """
    for item, freq in list(stats.items()):
        if freq < threshold:
            del stats[item]
            if freq < 0:
                big_stats[item] += freq
            else:
                big_stats[item] = freq


def replace_pair(pair, vocab, indices):
    """Replace all occurrences of a symbol pair ('A', 'B') with a new symbol 'AB'"""
    first, second = pair
    pair_str = ''.join(pair)
    pair_str = pair_str.replace('\\', '\\\\')
    changes = []
    pattern = re.compile(r'(?<!\S)' + re.escape(first + ' ' + second) + r'(?!\S)')
    for j, freq in indices[pair].items():
        if freq < 1:
            continue
        word, freq = vocab[j]
        new_word = ' '.join(word)
        new_word = pattern.sub(pair_str, new_word)
        new_word = tuple(new_word.split(' '))

        vocab[j] = (new_word, freq)
        changes.append((j, new_word, word, freq))

    return changes


def update_pair_statistics(pair, changed, stats, indices):
    """Minimally update the indices and frequency of symbol pairs

    if we merge a pair of symbols, only pairs that overlap with occurrences
    of this pair are affected, and need to be updated.
    """
    stats[pair] = 0
    indices[pair] = defaultdict(int)
    first, second = pair
    new_pair = first + second
    for j, word, old_word, freq in changed:
        # find all instances of pair, and update frequency/indices around it
        i = 0
        while True:
            # find first symbol
            try:
                i = old_word.index(first, i)
            except ValueError:
                break
            # if first symbol is followed by second symbol, we've found an occurrence of pair (old_word[i:i+2])
            if i < len(old_word) - 1 and old_word[i + 1] == second:
                # assuming a symbol sequence "A B C", if "B C" is merged, reduce the frequency of "A B"
                if i:
                    prev = old_word[i - 1:i + 1]
                    stats[prev] -= freq
                    indices[prev][j] -= 1
                if i < len(old_word) - 2:
                    # assuming a symbol sequence "A B C B", if "B C" is merged, reduce the frequency of "C B".
                    # however, skip this if the sequence is A B C B C, because the frequency of "C B" will be reduced by the previous code block
                    if old_word[i + 2] != first or i >= len(old_word) - 3 or old_word[i + 3] != second:
                        nex = old_word[i + 1:i + 3]
                        stats[nex] -= freq
                        indices[nex][j] -= 1
                i += 2
            else:
                i += 1

        i = 0
        while True:
            try:
                # find new pair
                i = word.index(new_pair, i)
            except ValueError:
                break
            # assuming a symbol sequence "A BC D", if "B C" is merged, increase the frequency of "A BC"
            if i:
                prev = word[i - 1:i + 1]
                stats[prev] += freq
                indices[prev][j] += 1
            # assuming a symbol sequence "A BC B", if "B C" is merged, increase the frequency of "BC B"
            # however, if the sequence is A BC BC, skip this step because the count of "BC BC" will be incremented by the previous code block
            if i < len(word) - 1 and word[i + 1] != new_pair:
                nex = word[i:i + 2]
                stats[nex] += freq
                indices[nex][j] += 1
            i += 1


def learn_bpe_codes(dsv, column: str, num_symbols: int,
                    min_frequency: int=2, total_symbols: bool=False,
                    verbose: bool=False) -> typing.List:
    """Learn num_symbols BPE operations from vocabulary, and write to outfile.
    """
    vocab = create_vocabulary(dsv, column)
    vocab = dict([(tuple(x[:-1]) + (x[-1] + '</w>',), y) for (x, y) in vocab.items()])
    sorted_vocab = sorted(vocab.items(), key=lambda x: x[1], reverse=True)

    stats, indices = get_pair_statistics(sorted_vocab)
    big_stats = copy.deepcopy(stats)
    codes = []
    if total_symbols:
        uniq_char_internal = set()
        uniq_char_final = set()
        for word in vocab:
            for char in word[:-1]:
                uniq_char_internal.add(char)
            uniq_char_final.add(word[-1])
        logging.info(f'Number of word-internal characters: {len(uniq_char_internal)}\n')
        logging.info(f'Number of word-final characters: {len(uniq_char_final)}\n')
        logging.info(f'Reducing number of merge operations by {len(uniq_char_internal)+len(uniq_char_final)}\n')
        num_symbols -= len(uniq_char_internal) + len(uniq_char_final)

    # threshold is inspired by Zipfian assumption, but should only affect speed
    threshold = max(stats.values()) / 10
    for i in tqdm.tqdm(range(num_symbols), disable=not verbose):
        if stats:
            most_frequent = max(stats, key=lambda x: (stats[x], x))

        # we probably missed the best pair because of pruning; go back to full statistics
        if not stats or (i and stats[most_frequent] < threshold):
            prune_stats(stats, big_stats, threshold)
            stats = copy.deepcopy(big_stats)
            most_frequent = max(stats, key=lambda x: (stats[x], x))
            # threshold is inspired by Zipfian assumption, but should only affect speed
            threshold = stats[most_frequent] * i / (i + 10000)
            prune_stats(stats, big_stats, threshold)

        if stats[most_frequent] < min_frequency:
            logging.info(f'no pair has frequency >= {min_frequency}. Stopping\n')
            break

        # logging.('pair {0}: {1} {2} -> {1}{2} (frequency {3})\n'.format(i, most_frequent[0], most_frequent[1], stats[most_frequent]))
        assert len(most_frequent) == 2
        codes.append(most_frequent)
        changes = replace_pair(most_frequent, sorted_vocab, indices)
        update_pair_statistics(most_frequent, changes, stats, indices)
        stats[most_frequent] = 0
        if not i % 100:
            prune_stats(stats, big_stats, threshold)
    return codes


class BPE:
    def __init__(self, codes: typing.Sequence, merges: int=-1,
                 separator: str='@@', ignore_list: typing.Sequence[str]=[]):
        self.bpe_codes = codes if merges == -1 else codes[:merges]

        # some hacking to deal with duplicates (only consider first instance)
        self.bpe_codes = dict([(code, i) for (i, code) in reversed(list(enumerate(self.bpe_codes)))])
        self.bpe_codes_reverse = dict([(pair[0] + pair[1], pair) for pair, i in self.bpe_codes.items()])

        self.separator = separator
        self.ignore_list = ignore_list
        self.cache = {}

    def process_line(self, line):
        """segment line, dealing with leading and trailing whitespace"""
        out = ""

        leading_whitespace = len(line) - len(line.lstrip('\r\n '))
        if leading_whitespace:
            out += line[:leading_whitespace]

        out += self.segment(line)

        trailing_whitespace = len(line) - len(line.rstrip('\r\n '))
        if trailing_whitespace and trailing_whitespace != len(line):
            out += line[-trailing_whitespace:]

        return out

    def segment(self, sentence):
        """segment single sentence (whitespace-tokenized string) with BPE encoding"""
        segments = self.segment_tokens(sentence.strip('\r\n ').split(' '))
        return ' '.join(segments)

    def segment_tokens(self, tokens):
        """segment a sequence of tokens with BPE encoding"""
        output = []
        for word in tokens:
            # eliminate double spaces
            if not word:
                continue
            new_word = [out for segment in self._isolate_ignore_list(word)
                        for out in self._encode(segment)]

            for item in new_word[:-1]:
                output.append(item + self.separator)
            output.append(new_word[-1])

        return output

    def _encode(self, orig):
        """Encode word based on list of BPE merge operations, which are applied consecutively
        """

        def get_pairs(word):
            """Return set of symbol pairs in a word.

            word is represented as tuple of symbols (symbols being variable-length strings)
            """
            pairs = set()
            prev_char = word[0]
            for char in word[1:]:
                pairs.add((prev_char, char))
                prev_char = char
            return pairs

        if orig in self.cache:
            return self.cache[orig]

        for glossary in self.ignore_list:
            if re.match('^' + glossary + '$', orig):
                self.cache[orig] = (orig,)
                return (orig,)

        word = tuple(orig[:-1]) + (orig[-1] + '</w>',)
        pairs = get_pairs(word)

        if not pairs:
            return orig

        while True:
            bigram = min(pairs, key=lambda pair: self.bpe_codes.get(pair, float('inf')))
            if bigram not in self.bpe_codes:
                break
            first, second = bigram
            new_word = []
            i = 0
            while i < len(word):
                try:
                    j = word.index(first, i)
                    new_word.extend(word[i:j])
                    i = j
                except:
                    new_word.extend(word[i:])
                    break

                if word[i] == first and i < len(word) - 1 and word[i + 1] == second:
                    new_word.append(first + second)
                    i += 2
                else:
                    new_word.append(word[i])
                    i += 1
            new_word = tuple(new_word)
            word = new_word
            if len(word) == 1:
                break
            else:
                pairs = get_pairs(word)

        # don't print end-of-word symbols
        if word[-1] == '</w>':
            word = word[:-1]
        elif word[-1].endswith('</w>'):
            word = word[:-1] + (word[-1].replace('</w>', ''),)

        self.cache[orig] = word
        return word

    def _isolate_ignored(self, word, ignored):
        """
        Isolate a ignored subword present inside a word.

        Returns a list of subwords. In which all 'ignored' subwords are isolated

        For example, if 'USA' is the ignored and '1934USABUSA' the word, the return value is:
            ['1934', 'USA', 'B', 'USA']
        """
        # regex equivalent of (if word == ignored or ignored not in word)
        if re.match('^' + ignored + '$', word) or not re.search(ignored, word):
            return [word]
        else:
            splits = re.split(ignored, word)
            segments = [segment.strip('\r\n ') for (n_split, split) in enumerate(splits[:-1]) for segment in [split, re.findall(ignored, word)[n_split]] if segment != '']
            return segments + [splits[-1].strip('\r\n ')] if splits[-1] != '' else segments

    def _isolate_ignore_list(self, word):
        word_segments = [word]
        for gloss in self.ignore_list:
            word_segments = [out_segments for segment in word_segments
                             for out_segments in self._isolate_ignored(segment, gloss)]
        return word_segments
