# coding: utf-8

import unittest
from pytools.preprocess.tokenize import tokenize


class TestTokenize(unittest.TestCase):
    def test_endpunct(self):
        sentence = "Je suis mort!"
        ref_tokens = ["Je", "suis", "mort", "!"]
        self.assertEqual(tokenize(sentence), ref_tokens)

    def test_startpunct(self):
        sentence = "...et ceci fût la fin"
        ref_tokens = ["...", "et", "ceci", "fût", "la", "fin"]
        self.assertEqual(tokenize(sentence), ref_tokens)

    def test_apos(self):
        sentence = "N'hésite surtout pas à t'engager avec 'Paul'"
        ref_tokens = ["N'", "hésite", "surtout", "pas", "à", "t'", "engager", "avec", "'", "Paul", "'"]
        self.assertEqual(tokenize(sentence), ref_tokens)

    def test_fulltokenize(self):
        sentence = "'Coucou' n'est pas un mot très formel... Qu'en penses-tu?"
        ref_tokens = ["'", "Coucou", "'", "n'", "est", "pas", "un", "mot", "très", "formel",
                      "...", "Qu'", "en", "penses", "-", "tu", "?"]
        self.assertEqual(tokenize(sentence), ref_tokens)


if __name__ == '__main__':
    unittest.main()
