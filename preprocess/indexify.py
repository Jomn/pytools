# coding: utf-8
""" Token-Index conversion tools """


def token_to_index(array, token2index, unknown=None):
    """ Replaces every token with its associated index.
    Args:
      array (list): List of tokens to indexify.
      token2index (dict): Dictionary used to associate each token with an index.
      unknown (str, opt): Token used to replace an unknown token.
    """
    return [token2index[token] if token in token2index else token2index[unknown] for token in array]


def index_to_token(array, index2token):
    return [index2token[index] for index in array]
