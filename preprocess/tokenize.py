# coding: utf-8

import re

punctend_regex = re.compile(r'(\w+)(\W+)$')
punctstart_regex = re.compile(r'^(\W+)(\w+)')
punctmid_regex = re.compile(r"(\w)([^\w_' -]+)(\w)")
apos_regex = re.compile(r'(\w+\')(\w+)')
hyphen_regex = re.compile(r'(\w+)(-)(\w+)')
multspace_regex = re.compile(r'\s+')


def tokenize(text):
    text = text.strip(' ')
    base_tokens = re.split(multspace_regex, text)
    tokens = []
    for base_token in base_tokens:
        base_token = re.sub(punctend_regex, r'\1 \2', base_token)
        base_token = re.sub(punctstart_regex, r'\1 \2', base_token)
        base_token = re.sub(punctmid_regex, r'\1 \2 \3', base_token)
        base_token = re.sub(apos_regex, r'\1 \2', base_token)
        base_token = re.sub(hyphen_regex, r'\1 \2 \3', base_token)
        base_token = re.sub(multspace_regex, ' ', base_token)
        base_token = base_token.strip(' ')
        if not base_token:
            continue
        subtokens = base_token.split(' ')
        tokens.extend(subtokens)
    return tokens
