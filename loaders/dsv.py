# coding: utf-8
""" Read nonstrict DSV files """

import sys
from itertools import zip_longest
from collections import defaultdict
import logging
from contextlib import contextmanager
import gzip
import bidict  # type:ignore
import typing as tp


@contextmanager
def open_with_default(filepath, mode):
    if filepath is None:
        if mode == 'r':
            yield sys.stdin
        elif mode == 'w':
            yield sys.stdout
    else:
        if filepath.endswith(".gz"):
            logging.debug("Detected GZIP file.")
            if mode in ['r', 'w', 'a']:
                mode += 't'
            with gzip.open(filepath, mode, encoding='utf-8') as f:
                yield f
        else:
            with open(filepath, mode, encoding='utf-8') as f:
                yield f


class Dsv:
    """ DSV class that allows parsing of DSV files """

    def __init__(self, filepath: str, delimiter: str = '\t'):
        self.filepath: str = filepath
        self.delimiter: str = delimiter
        self._length: tp.Optional[int] = None
        self._length_empty: tp.Optional[int] = None
        logging.debug("Reading DSV header from '{}'".format(filepath))
        with open_with_default(filepath, 'r') as fin:
            header = next(fin).rstrip('\r\n')
            self._fieldnames: tp.List[str] = header.split(delimiter)

    @property
    def fieldnames(self):
        return list(self._fieldnames)

    def __len__(self) -> int:
        """ Returns the number of rows in the file.
        Note: It doesn't take into account empty lines.
        """
        if self._length is None:
            self._length = self._get_length()
        return self._length

    def size(self, read_empty: bool = False):
        if not read_empty:
            return len(self)
        if self._length_empty is None:
            self._length_empty = self._get_length(True)
        return self._length_empty

    def _get_length(self, read_empty=False):
        return sum(1 for _ in self.read(read_empty=read_empty))

    def read(self, field_filter: tp.List[str] = [],
             read_empty: bool = False,
             warn: bool = False) -> tp.Iterator[tp.Dict[str, tp.Optional[str]]]:
        """ Read the DSV file using a generator.
        Args:
          field_filter (list): If set, only return values for these fields.
          read_empty (bool): If True, also return something for empty lines (as only Nones)
          warn (bool): If True, logs warnings if a line is too long or too short (except empty)
        """
        def dictify_shorter_or_equal_row(fields, fieldnames):
            row = dict(zip_longest(self._fieldnames, fields), fillvalue=None)
            if warn and 0 < len(fields) < len(self._fieldnames):
                logging.warning("Too few fields: Line {} has {} fields!".format(line_nb,
                                                                                len(fields)))
            return row

        def dictify_longer_row(fields, fieldnames):
            restfields = fields[len(self._fieldnames):]
            row = dict(zip(self._fieldnames, fields))
            row['restfields'] = restfields
            if warn:
                logging.warning("Too many fields: Line {} has {} fields:".format(line_nb,
                                                                                 len(fields)))
            return row

        def filter_row(field_filter, row):
            return {fkey: row[fkey] for fkey in field_filter}

        with open_with_default(self.filepath, 'r') as fin:
            next(fin)
            line_nb = 0
            for line in fin:
                line_nb += 1
                line = line.rstrip('\r\n')
                if not line and not read_empty:
                    continue
                fields = [None if field == '' else field for field in line.split(self.delimiter)]
                if fields.count(None) == len(fields) and not read_empty:
                    continue

                if len(self._fieldnames) >= len(fields):
                    row = dictify_shorter_or_equal_row(fields, self._fieldnames)
                else:
                    row = dictify_longer_row(fields, self._fieldnames)

                if field_filter:
                    row = filter_row(field_filter, row)

                yield row

    def read_indexify(self, tokensets, unknowns, field_filter=[], read_empty=False,
                      warn=False, lowercase=False):
        """ Reads a DSV file and replaces tokens with indexes when a token to index dictionary is available.
        Args:
          tokensets (dict): Dictionary of tokensets. The keys must be valid field names.
          unknowns (dict): Dictionary of strings used to replace unknown tokens.
                           The keys must match the ones in tokensets.
          field_filter (list): If set, only return values for these fields.
          read_empty (bool): If True, also return something for empty lines (as only Nones)
          warn (bool): If True, logs warnings if a line is too long or too short (except empty)
          lowercase (bool): If True, lowercases every character.
        """
        for row in self.read(field_filter, read_empty, warn):
            for fieldname in tokensets:
                token = row[fieldname]
                if lowercase:
                    token = token.lower()
                tokenset = tokensets[fieldname]
                row[fieldname] = tokenset[token if token in tokenset else unknowns[fieldname]]
            yield row

    def read_all(self, field_filter: tp.List[str] = [],
                 read_empty: bool = False,
                 warn: bool = False) -> tp.Dict[str, tp.List[tp.Optional[str]]]:
        """ Read all the DSV file.
        Args:
          field_filter (list): If set, only return values for these fields.
          read_empty (bool): If True, also return something for empty lines (as only Nones)
          warn (bool): If True, logs warnings if a line is too long or too short (except empty)

        Returns a dictionary of lists where the keys are the fieldnames.
        """
        rows: tp.Dict[str, tp.List[tp.Optional[str]]] = defaultdict(list)
        fieldnames = self._fieldnames if not field_filter else field_filter
        for row in self.read(field_filter, read_empty, warn):
            for field in fieldnames:
                rows[field].append(row[field])
        return rows

    def read_all_rows(self, field_filter: tp.List[str] = [],
                      read_empty: bool = False,
                      warn: bool = False) -> tp.List[tp.Dict[str, tp.Optional[str]]]:
        """ Read all the DSV file row by row.
        Args:
          field_filter (list): If set, only return values for these fields.
          read_empty (bool): If True, also return something for empty lines (as only Nones)
          warn (bool): If True, logs warnings if a line is too long or too short (except empty)

        Returns a list of dictonaries where the dictionaries are the rows in the file.
        """
        return [row for row in self.read(field_filter, read_empty, warn)]

    def _write_row(self, fout, row, write_empty):
        if row.count('') == len(row) and write_empty:
            print(file=fout)
        else:
            print(self.delimiter.join(row), file=fout)

    def writer(self, filepath: str, fieldnames: tp.List[str],
               write_empty: bool = False,
               overwrite_fieldnames: bool = False):
        """ Coroutine to write data row by row in the DSV by updating and/or adding new field.
        Args:
          filepath (str): Path to the output file, if None outputs in stdout. Can write gzipped files.
          fieldnames (list): The field names of the associated data.
          write_empty (bool): If True, writes an empty line if only Nones are present.
          overwrite_fieldnames (bool) : If true, kept fields will be in the same order as specified in filednames.

        Note: Send dictionaries of values where the keys are the fieldnames to update.
        Note: If None is sent, row is removed
        Note: When overwrite_fieldnames is False, only new fieldnames need to be specified and will be appended.
        """
        def xstr(s):
            if s is None:
                return ''
            return str(s)

        if not overwrite_fieldnames:
            new_fieldnames = list(self._fieldnames)
            for new_field in fieldnames:
                if new_field not in new_fieldnames:
                    new_fieldnames.append(new_field)
        else:
            new_fieldnames = fieldnames

        with open_with_default(filepath, 'w') as fout:
            print(self.delimiter.join(new_fieldnames), file=fout)
            for row_idx, row in enumerate(self.read(read_empty=write_empty)):
                new_data = yield
                if new_data is None:
                    continue
                for field in fieldnames:
                    if field in new_data:
                        row[field] = new_data[field]
                    elif field not in new_data and field not in self._fieldnames:
                        row[field] = None
                new_row = [xstr(row[field]) for field in new_fieldnames]
                self._write_row(fout, new_row, write_empty)

    def writer_unindexify(self, filepath, fieldnames, idx2tokens, write_empty=False):
        """
        """
        def xstr(s):
            if s is None:
                return ''
            return str(s)

        new_fieldnames = list(self._fieldnames)
        for new_field in fieldnames:
            if new_field not in new_fieldnames:
                new_fieldnames.append(new_field)

        with open_with_default(filepath, 'w') as fout:
            print(self.delimiter.join(new_fieldnames), file=fout)
            for row_idx, row in enumerate(self.read(read_empty=write_empty)):
                new_data = yield
                for fieldname in idx2tokens:
                    new_data[fieldname] = idx2tokens[fieldname][new_data[fieldname]]
                for field in fieldnames:
                    row[field] = new_data[field]
                new_row = [xstr(row[field]) for field in new_fieldnames]
                self._write_row(fout, new_row, write_empty)

    def write_all(self, filepath, data, fieldnames, write_empty=False):
        """ Write the DSV by updating and/or adding new fields.
        Args:
          filepath (str): Path to the output file, if None outputs in stdout. Can write gzipped files.
          data (dict of lists): The data to add/update.
          fieldnames (list): The field names of the associated data.
          write_empty (bool): If True, writes an empty line if only Nones are present.
        """
        writer = self.writer(filepath, fieldnames, write_empty)
        next(writer)
        data_length = len(data[fieldnames[0]])
        full_iteration = False
        for data_idx in range(data_length):
            new_row_data = {fieldname: data[fieldname][data_idx] for fieldname in fieldnames}
            #writer.send(new_row_data)
            try:
                writer.send(new_row_data)
            except StopIteration:
                full_iteration = True
                if data_idx + 1 < data_length:
                    logging.error("Data to write is longer than the original data (stopped at idx {})".format(data_idx))
                break
        writer.close()
        if not full_iteration:
            logging.error("Data to write was shorter than the original data")

    def write_all_unindexify(self, filepath, data, fieldnames, idx2tokens, write_empty=False):
        """ Write the DSV by updating and/or adding new fields and by converting indexes into tokens.
        Args:
          filepath (str): Path to the output file, if None outputs in stdout. Can write gzipped files.
          data (dict of lists): The data to add/update.
          fieldnames (list): The field names of the associated data.
          idx2tokens (dict of dict): Dict of idx2token dicts. The keys are the names of the fields.
          write_empty (bool): If True, writes an empty line if only Nones are present.
        """
        writer = self.writer(filepath, fieldnames, write_empty)
        next(writer)
        data_length = len(data[fieldnames[0]])
        full_iteration = False
        for data_idx in range(data_length):
            new_row_data = {fieldname: data[fieldname][data_idx] for fieldname in fieldnames}
            for fieldname in idx2tokens:
                new_row_data[fieldname] = idx2tokens[fieldname][new_row_data[fieldname]]
            try:
                writer.send(new_row_data)
            except StopIteration:
                full_iteration = True
                if data_idx + 1 < data_length:
                    logging.error("Data to write is longer than the original data (stopped at idx {})".format(data_idx))
                break
        writer.close()
        if not full_iteration:
            logging.error("Data to write was shorter than the original data")

    def count_occurrences(self, column: str, value: str) -> int:
        """ Count the number of occurences of a value in a column.
        Args:
          column (str): Name of the column.
          value (str): Value to count.

        Returns the number of occurences of the value.
        """
        return sum(1 if row[column] == value else 0 for row in self.read(field_filter=[column]))

    def count_uniques(self, column: str) -> int:
        """ Count the number of unique tokens.
        Args:
          column (str): Name of the column.

        Returns the number of unique tokens in the column.
        """

        tokens = set()
        for row in self.read(field_filter=[column]):
            tokens.add(row[column])

        return len(tokens)

    def uniques(self, column: str,
                return_counts: bool = False) -> tp.Union[tp.Dict[str, int], tp.Set[str]]:
        """ Get all the unique tokens from the column.
        Args:
          column (str): Name of the column.
          return_counts (bool, optional): If true, also the return the number of occurences of each token.
        """

        tokens: tp.DefaultDict[str, int] = defaultdict(int)
        for row in self.read(field_filter=[column]):
            token: tp.Optional[str] = row[column]
            if token is None:
                continue
            tokens[token] += 1

        return tokens if return_counts else set(tokens.keys())

    def index_column(self, column, word2index=None, extratokens=[], start=1):
        """ Creates an index for all the tokens in the column.
        Args:
          column (str): Name of the column.
          word2index (dict): Provides an already existing word index.
                             If None, a new word index dict is created.
        """
        if word2index is None:
            word2index = bidict.bidict()
        else:
            word2index = bidict.bidict(word2index)
        for row in self.read(field_filter=[column]):
            try:
                word2index.put(row[column], start + len(word2index))
            except bidict.KeyDuplicationError:
                pass

        for extratoken in extratokens:
            try:
                word2index.put(extratoken, start + len(word2index))
            except bidict.KeyDuplicationError:
                pass

        return word2index
