# coding: utf-8
""" Module which contains basic functions to load sets and dictionaries from files.
These files must have one token per line.
"""

from bidict import bidict


def load_set(pathname, lowercase=False, dtype=str, extratokens=[]):
    """ Loads a set from a file.
    Args:
      pathname (str): Path to the file.
      lowercase (bool, opt): Should everything be lowercased ?
      dtype (func): Type of the data to store.
      extratokens (iterable): Additional tokens to add in.
    """
    tokenset = set()
    with open(pathname, 'r', encoding='utf-8') as fin:
        for line in fin:
            line = line.rstrip("\r\n")
            if not line:
                continue
            if lowercase:
                line = line.lower()
            token = dtype(line)
            tokenset.add(token)
    for token in extratokens:
        token = dtype(token)
        tokenset.add(token)

    return tokenset


def load_dict(pathname, lowercase=False, dtype=str, start=0, extratokens=[]):
    """ Loads a dictionary from a file.
    Args:
      pathname (str): Path to the file.
      lowercase (bool, opt): Should everything be lowercased ?
      dtype (func, opt): Type of the data to store.
      start (int, opt): Start index of the dict.
      extratokens (iterable): Additional tokens to add in.
    """
    token2idx = {}
    idx2token = {}
    with open(pathname, 'r', encoding='utf-8') as fin:
        for line in fin:
            line = line.rstrip("\r\n")
            if not line:
                continue
            if lowercase:
                line = line.lower()
            token = dtype(line)
            if token not in token2idx:
                idx = len(token2idx) + start
                idx2token[idx] = token
                token2idx[token] = idx
    for token in extratokens:
        token = dtype(token)
        if token not in token2idx:
            idx = len(token2idx) + start
            token2idx[token] = idx
            idx2token[idx] = token

    return token2idx, idx2token


def load_bidict(pathname, lowercase=False, dtype=str, start=0, extratokens=[]):
    """ Loads a dictionary from a file.
    Args:
      pathname (str): Path to the file.
      lowercase (bool, opt): Should everything be lowercased ?
      dtype (func, opt): Type of the data to store.
      start (int, opt): Start index of the dict.
      extratokens (iterable): Additional tokens to add in.
    """
    tokensdict = bidict()
    with open(pathname, 'r', encoding='utf-8') as fin:
        for line in fin:
            line = line.rstrip("\r\n")
            if not line:
                continue
            if lowercase:
                line = line.lower()
            token = dtype(line)
            if token not in tokensdict:
                idx = len(tokensdict) + start
                tokensdict[token] = idx
    for token in extratokens:
        token = dtype(token)
        if token not in tokensdict:
            idx = len(tokensdict) + start
            tokensdict[token] = idx

    return tokensdict
