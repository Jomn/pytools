# coding: utf-8
""" This module contains classes useful to parse MCF files. """

from collections import defaultdict
import logging


def type_converter(value, type_name, strict=False):
    """ Converts value into the given type """
    if type_name == "INT":
        return int(value)
    if type_name == "FLOAT":
        return float(value)
    if type_name == "VOCAB" or type_name == "EMB" or type_name == "STRING" or not strict:
        return str(value)

    raise RuntimeError("MCD file has an unknown type: {}".format(type_name))


class McdReader:
    """Parser class used to read MCD files.

    Attributes:
       columns (dict of (int, dict)): Dictionary describing the role of each column in a MCF file
       name2column (dict of (str, int)): Dictonary that stores from a column name the column id
       max_col (int): Maximum column id that is needed in a MCF file
    """
    def __init__(self, filename=None):
        """
        Parser for the MCD file.
        By default, the WPLGFS descriptor is used.
        """
        self.columns = {}
        self.name2column = {}
        self.max_col = 0
        if filename is None:
            self._wplgfs()
            return
        self._load(filename)

    def _wplgfs(self):
        self.columns[1] = {'name': "FORM",
                           'type': "VOCAB",
                           'info': None}
        self.name2column["FORM"] = 1
        self.columns[2] = {'name': "POS",
                           'type': "VOCAB",
                           'info': None}
        self.name2column["POS"] = 2
        self.columns[3] = {'name': "LEMMA",
                           'type': "VOCAB",
                           'info': None}
        self.name2column["LEMMA"] = 3
        self.columns[4] = {'name': "GOV",
                           'type': "INT",
                           'info': None}
        self.name2column["GOV"] = 4
        self.columns[5] = {'name': "LABEL",
                           'type': "VOCAB",
                           'info': None}
        self.name2column["LABEL"] = 5
        self.columns[6] = {'name': "SEG",
                           'type': "INT",
                           'info': None}
        self.name2column["SENT_SEG"] = 6
        self.max_col = 6

    def _load(self, filename):
        with open(filename, "r") as mcd_in:
            for line in mcd_in:
                line = line.rstrip(" \r\n")
                if not line or line.startswith('#'):
                    continue
                tokens = line.split(" ")
                if len(tokens) != 4:
                    raise RuntimeError("MCD file has an invalid number of columns per line!")
                try:
                    column_id = int(tokens[0])
                except:
                    raise RuntimeError("First column in MCD file is not a integer !")
                if column_id in self.columns:
                    raise RuntimeError("MCD file has multiple occurences of the same column id !")
                if column_id > self.max_col:
                    self.max_col = column_id
                column_name = tokens[1]
                if column_name in self.name2column:
                    raise RuntimeError("Two columns share the same name in the MCD file !")
                column_type = tokens[2]
                column_info = tokens[3] if tokens[3] != "_" else None
                self.columns[column_id] = {'name': column_name,
                                           'type': column_type,
                                           'info': column_info}
                self.name2column[column_name] = column_id

    def __contains__(self, key):
        return key in self.name2column

    def get_column_name(self, column_id):
        """ Fetchs the name of the specified column id """
        return self.columns[column_id]['name']

    def get_column_type(self, column_id):
        """ Fetchs the type of the specified column id """
        return self.columns[column_id]['type']

    def get_column_info(self, column_id):
        """ Fetchs the info of the specified column id """
        return self.columns[column_id]['info']

    def get_column_id(self, column_name):
        """ Fetchs the column id from a column name """
        return self.name2column[column_name]

    def get_column_type_from_name(self, column_name):
        """ Fetchs the column type from a column name """
        return self.columns[self.get_column_id(column_name)]['type']

    def get_column_info_from_name(self, column_name):
        """ Fetchs the column info from a column name """
        return self.columns[self.get_column_id(column_name)]['info']


class McfReader:
    """ Parser class used to read MCF files

    Args:
       mcf_filename (str): The name of the MCF file.
       mcd_filename (str, optional): The name of the MCD file.
       encoding (str, optional): Encoding of the MCF file.
       strict (bool, optional): If True, doesn't allow a column to have unknown types.

    Attributes:
       mcd (McdReader): Mcd object used to understand the meaning of the columns in the MCF file
       filename (str): File name of the MCF file
       encoding (str): Encoding of the MCF file. Defaults to UTF-8.
    """
    def __init__(self, mcf_filename, mcd_filename=None,
                 encoding=None, strict=False):
        self.mcd = McdReader(mcd_filename)
        self.filename = mcf_filename
        self.encoding = encoding
        self._len, self.nb_sentences = self._length()
        self.strict = strict

    def __len__(self):
        return self._len

    def _length(self):
        with open(self.filename, "r") as fin:
            idx = 0
            nb_sentences = 0
            for line in fin:
                line = line.rstrip("\r\n")
                if line:
                    idx += 1
                    if "SEG" in self.mcd:
                        seg = int(line.split("\t")[self.mcd.name2column["SEG"]-1])
                        if seg == 1:
                            nb_sentences += 1
        return idx, nb_sentences

    def segments(self, segment_column="SEG"):
        segments = defaultdict(list)
        end_segment = False
        linenb = 0
        with open(self.filename, 'r') as fin:
            for line in fin:
                linenb += 1
                line = line.rstrip("\r\n")
                if not line:
                    logging.warning("An empty line was found in '%s' ! (line %d)",
                                    self.filename, linenb)
                    continue
                tokens = line.split("\t")
                if len(tokens) < self.mcd.max_col:
                    raise RuntimeError("MCF file {} has fewer columns than specified in the MCD ! (line {})".format(self.filename, linenb))
                for column_name, column_id in self.mcd.name2column.items():
                    column_type = self.mcd.get_column_type(column_id)
                    value = type_converter(tokens[column_id-1], column_type, strict=self.strict)
                    segments[column_name].append(value)
                    if (segment_column in self.mcd and
                            column_name == segment_column and
                            int(value) == 1):
                        end_segment = True
                if end_segment:
                    yield segments
                    segments = defaultdict(list)
                    end_segment = False
            if segments:
                if segment_column in self.mcd:
                    logging.warning("MCF file '%s' ends without any end of segment.",
                                    self.filename)
                yield segments

    def lines(self):
        with open(self.filename, "r", encoding=self.encoding) as fin:
            linenb = 0
            for line in fin:
                linenb += 1
                line = line.rstrip("\r\n")
                if not line:
                    logging.warning("An empty line was found in '%s' ! (line %d)",
                                    self.filename, linenb)
                    continue
                tokens = line.split("\t")
                if len(tokens) < self.mcd.max_col:
                    raise RuntimeError("MCF file {} has fewer columns than specified in the MCD ! (line {})".format(self.filename, linenb))
                item = {}
                for column_name, column_id in self.mcd.name2column.items():
                    column_type = self.mcd.get_column_type(column_id)
                    value = type_converter(tokens[column_id-1], column_type, strict=self.strict)
                    item[column_name] = value
                yield item

    def read_all(self):
        """ Reads the entire MCF file

        Returns:
            list of dict of list: The sentences divided by column.
        """
        sentences = defaultdict(list)
        for segments in self:
            for column_name, column in segments:
                sentences[column_name].append(column)

        return sentences


# class McfWriter:
#     """ Writer class used to dump MCF files from arrays.
#     """

#     def __init__(self, pathname, original_mcf=None, original_mcd=None,
#                  mode='):
#         pass
