import numpy as np


def group_to_list(group, keys, remove_path=False, raise_nonexistant=True):
    def add_dataset_to_list(name, dataset):
        if remove_path:
            name = name.split('/')[-1]
        if name in keys:
            datasets[name] = dataset
    if raise_nonexistant and not all(key in group for key in keys):
        raise RuntimeError("Not all asked keys are in the h5 group!")
    datasets = {}
    group.visititems(add_dataset_to_list)
    return [datasets[key] for key in keys]


def merge_datasets(datasets, max_len=None, pad_value=0, cut_end=True):
    concat = None
    for dataset in datasets:
        if concat is None:
            if max_len is not None:
                start = max(0, len(dataset) - max_len)
                dataset = dataset[:max_len] if cut_end else dataset[start:]
            concat = np.expand_dims(dataset, 0)
            continue
        lengths = []
        for dim in range(len(dataset.shape)):
            if dim == 0 and max_len is not None:
                length = min(max(dataset.shape[dim], concat.shape[dim + 1]), max_len)
            else:
                length = max(dataset.shape[dim], concat.shape[dim + 1])
            lengths.append(length)
        pad_shapes = []
        for dim, length in enumerate(lengths):
            diff = max(length - dataset.shape[dim], 0)
            pad_shapes.append((0, diff))
        padded = np.pad(dataset, pad_shapes, 'constant', constant_values=pad_value)
        pad_shapes = [(0, 0)]
        for dim, length in enumerate(lengths):
            diff = max(length - concat.shape[dim + 1], 0)
            pad_shapes.append((0, diff))
        concat = np.pad(concat, pad_shapes, 'constant', constant_values=pad_value)
        if max_len is not None:
            start = max(0, len(padded) - max_len)
            padded = padded[:max_len] if cut_end else padded[start:]
        concat = np.concatenate((concat, np.expand_dims(padded, 0)), axis=0)
    return concat


def truncate_datasets(group, samples_to_keep):
    def resize_dataset(name, dataset):
        dataset.resize(samples_to_keep, axis=0)
    group.visititems(resize_dataset)


def shuffle_indices(size):
    return np.random.permutation(size)


def make_batch(dataset_list, batch_size, batch_start, indices=None):
    batch_list = []
    for dataset in dataset_list:
        if indices is None:
            batch_list.append(dataset[batch_start:batch_start + batch_size])
        else:
            batch_indices = indices[batch_start:batch_start:batch_size]
            sorted_argidx = np.argsort(batch_indices)
            batch = dataset[batch_indices[sorted_argidx]]
            unsorted_argidx = np.argsort(sorted_argidx)
            batch_list.append(batch[unsorted_argidx])

    return batch_list
