#!/usr/bin/env python
# coding: utf-8

import argparse
import logging
from typing import List, Optional, Dict, Tuple
from collections import Counter

from pytools.loaders.dsv import Dsv  # type: ignore


class CommaSeparatorAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        tokens = values.split(',')
        setattr(namespace, self.dest, tokens)


def argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('dsv1')
    parser.add_argument('dsv2')
    parser.add_argument('-1', '--columns-dsv1', default=[],
                        action=CommaSeparatorAction,
                        help="Column names to keep in the first DSV file")
    parser.add_argument('-2', '--columns-dsv2', default=[],
                        action=CommaSeparatorAction,
                        help="Column names to keep in the second DSV file")
    parser.add_argument('-j', '--join-column')
    parser.add_argument('-O', '--column-order', action=CommaSeparatorAction,
                        help="Append each fieldname with '_c' where c is the dsv file number")
    parser.add_argument('-r', '--rename-columns', action=CommaSeparatorAction,
                        help="New name of the inserted columns")
    # parser.add_argument('-o', '--output')
    parser.add_argument('--delimiter', default="\t")
    parser.add_argument('--delimiter-dsv1')
    parser.add_argument('--delimiter-dsv2')
    parser.add_argument('-l', '--logger', default='INFO',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help="Logging level: DEBUG, INFO (default), WARNING, ERROR")
    args = parser.parse_args()

    numeric_level = getattr(logging, args.logger.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(args.logger))
    logging.basicConfig(level=numeric_level)

    return args


def post_argparser(args):
    if args.delimiter_dsv1 is None:
        args.delimiter_dsv1 = args.delimiter
    if args.delimiter_dsv2 is None:
        args.delimiter_dsv2 = args.delimiter


def get_fieldnames(fieldnames1: List[str],
                   fieldnames2: List[str],
                   fieldname_order: Optional[List[str]] = None,
                   rename_fieldnames: Optional[List[str]] = None) -> Tuple[List[str], Dict[str, str], Dict[str, str]]:
    if fieldname_order is None:
        fieldname_order = [fieldname + "_1" for fieldname in fieldnames1] + [fieldname + "_2" for fieldname in fieldnames2]
    if rename_fieldnames is None:
        rename_fieldnames = ["_"] * len(fieldname_order)

    if len(fieldnames1) + len(fieldnames2) != len(fieldname_order):
        raise RuntimeError("Number of columns to write does not match with the given column order.")
    if len(fieldname_order) != len(rename_fieldnames):
        raise RuntimeError("Number of column new names does not match with the number of columns.")

    newnames1: Dict[str, str] = {}
    newnames2: Dict[str, str] = {}
    renamed_fieldnames: List[str] = []
    for fieldname, newname in zip(fieldname_order, rename_fieldnames):
        file_num: int = int(fieldname[-1])
        basename: str = fieldname[:-2]
        if newname == "_":
            newname = basename
        renamed_fieldnames.append(newname)
        if file_num == 1:
            newnames1[basename] = newname
        elif file_num == 2:
            newnames2[basename] = newname
        else:
            raise RuntimeError("Unknown file number given in column order enumaration.")

    counts: Counter = Counter(renamed_fieldnames)
    for fieldname, count in counts.most_common():
        if count <= 1:
            break
        num: int = 1
        while True:
            try:
                idx: int = renamed_fieldnames.index(fieldname)
                newname = fieldname + "_" + str(num)
                renamed_fieldnames[idx] = newname
                basename = fieldname_order[idx][:-2]
                if num == 1:
                    newnames1[basename] = newname
                elif num == 2:
                    newnames2[basename] = newname
                else:
                    raise RuntimeError("Too many columns have the same renamed name!")
                num += 1
            except ValueError:
                break

    return renamed_fieldnames, newnames1, newnames2


def print_header(fieldnames: List[str], delimiter: str):
    print(delimiter.join(fieldnames))


def print_row(fieldnames: List[str],
              row1: Dict[str, str],
              row2: Dict[str, str],
              newnames1: Dict[str, str],
              newnames2: Dict[str, str],
              delimiter: str):
    row = {newnames1[fieldname]: row1[fieldname] for fieldname in row1}
    row.update({newnames2[fieldname]: row2[fieldname] for fieldname in row2})

    print(delimiter.join([row[fieldname] for fieldname in fieldnames]))


def main():
    args = argparser()
    post_argparser(args)

    dsv1: Dsv = Dsv(args.dsv1, args.delimiter_dsv1)
    dsv2: Dsv = Dsv(args.dsv2, args.delimiter_dsv2)

    fieldnames1: List[str] = dsv1.fieldnames if not args.columns_dsv1 else args.columns_dsv1
    fieldnames2: List[str] = dsv2.fieldnames if not args.columns_dsv2 else args.columns_dsv2

    fieldnames, newnames1, newnames2 = get_fieldnames(fieldnames1, fieldnames2,
                                                      args.column_order,
                                                      args.rename_columns)

    print_header(fieldnames, args.delimiter)
    filter_fields2 = fieldnames2
    filter_fields1 = fieldnames1
    if args.join_column is not None:
        filter_fields1 = fieldnames1 if args.join_column in fieldnames1 else fieldnames1 + [args.join_column]
        filter_fields2 = fieldnames2 if args.join_column in fieldnames2 else fieldnames2 + [args.join_column]
    row2_reader = dsv2.read(field_filter=filter_fields2)
    for row1 in dsv1.read(field_filter=filter_fields1):
        row2 = next(row2_reader)
        if args.join_column is not None:
            while row1[args.join_column] != row2[args.join_column]:
                try:
                    row2 = next(row2_reader)
                except StopIteration:
                    raise RuntimeError("DSV2 doesn't contain all the rows from DSV1")
        if args.join_column is not None and args.join_column not in fieldnames1:
            del row1[args.join_column]
        if args.join_column is not None and args.join_column not in fieldnames2:
            del row2[args.join_column]
        print_row(fieldnames, row1, row2, newnames1, newnames2, args.delimiter)


if __name__ == '__main__':
    main()
