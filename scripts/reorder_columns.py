#!/usr/bin/env python
# coding: utf-8

import argparse
import logging

from pytools.loaders.dsv import Dsv  # type:ignore


def argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('dsv')
    parser.add_argument('fieldname', nargs='+')
    parser.add_argument('-d', '--delimiter', default="\t")
    parser.add_argument('-l', '--logger', default='INFO',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help="Logging level: DEBUG, INFO (default), WARNING, ERROR")
    args = parser.parse_args()

    numeric_level = getattr(logging, args.logger.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(args.logger))
    logging.basicConfig(level=numeric_level)

    return args


def main():
    args = argparser()

    dsv = Dsv(args.dsv, delimiter=args.delimiter)

    reordered = args.fieldname

    if not set(reordered) <= set(dsv.fieldnames):
        raise RuntimeError("Reordered fieldnames must all appear in the original")

    writer = dsv.writer(None, reordered, overwrite_fieldnames=True)
    next(writer)

    for row in dsv.read():
        try:
            writer.send(row)
        except StopIteration:
            pass


if __name__ == '__main__':
    main()
